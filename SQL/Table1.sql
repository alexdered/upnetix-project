-- --------------------------------------------------------
-- Хост:                         127.0.0.1
-- Версия на сървъра:            10.3.15-MariaDB - mariadb.org binary distribution
-- ОС на сървъра:                Win64
-- HeidiSQL Версия:              9.5.0.5196
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;


-- Dumping database structure for teacherapp
CREATE DATABASE IF NOT EXISTS `teacherapp` /*!40100 DEFAULT CHARACTER SET latin1 */;
USE `teacherapp`;

-- Дъмп структура за таблица teacherapp.authorities
CREATE TABLE IF NOT EXISTS `authorities` (
  `username` varchar(50) DEFAULT NULL,
  `authority` varchar(50) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Дъмп данни за таблица teacherapp.authorities: ~39 rows (approximately)
/*!40000 ALTER TABLE `authorities` DISABLE KEYS */;
INSERT INTO `authorities` (`username`, `authority`) VALUES
	('alex21', 'ROLE_USER'),
	('Arnie', 'ROLE_USER'),
	('alex21', 'ROLE_TEACHER'),
	('alex21', 'ROLE_ADMIN'),
	('Arnie', 'ROLE_TEACHER'),
	('pant', 'ROLE_USER'),
	('pant1', 'ROLE_USER'),
	('pant2', 'ROLE_USER'),
	('pant3', 'ROLE_USER'),
	('pant4', 'ROLE_USER'),
	('pant5', 'ROLE_USER'),
	('pant6', 'ROLE_USER'),
	('pant7', 'ROLE_USER'),
	('teach', 'ROLE_TEACHER'),
	('teach', 'ROLE_USER'),
	('pant8', 'ROLE_TEACHER'),
	('pant8', 'ROLE_USER'),
	('pant9', 'ROLE_TEACHER'),
	('pant9', 'ROLE_USER'),
	('aleks33', 'ROLE_TEACHER'),
	('aleks33', 'ROLE_USER'),
	('alex22', 'ROLE_USER'),
	('alex144', 'ROLE_TEACHER'),
	('alex144', 'ROLE_USER'),
	('pantwww', 'ROLE_TEACHER'),
	('pantwww', 'ROLE_USER'),
	('didig', 'ROLE_TEACHER'),
	('didig', 'ROLE_USER'),
	('MartinM', 'ROLE_TEACHER'),
	('MartinM', 'ROLE_USER'),
	('donUser', 'ROLE_TEACHER'),
	('donUser', 'ROLE_USER'),
	('SisiG', 'ROLE_TEACHER'),
	('SisiG', 'ROLE_USER'),
	('pesho33', 'ROLE_USER'),
	('ivan85', 'ROLE_USER'),
	('Icko', 'ROLE_USER'),
	('alexdered', 'ROLE_TEACHER'),
	('alexdered', 'ROLE_USER');
/*!40000 ALTER TABLE `authorities` ENABLE KEYS */;

-- Дъмп структура за таблица teacherapp.comments
CREATE TABLE IF NOT EXISTS `comments` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(50) NOT NULL,
  `comment` varchar(300) NOT NULL,
  `course_id` int(11) NOT NULL,
  `time` varchar(50) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=131 DEFAULT CHARSET=latin1;

-- Дъмп данни за таблица teacherapp.comments: ~21 rows (approximately)
/*!40000 ALTER TABLE `comments` DISABLE KEYS */;
INSERT INTO `comments` (`id`, `username`, `comment`, `course_id`, `time`) VALUES
	(110, 'pant7', 'Here I am still awake!', 38, '2019/08/13 02:09:44'),
	(111, 'pant7', 'Cool story bro :)', 38, '2019/08/13 02:10:01'),
	(112, 'alex21', 'Zagorka', 38, '2019/08/13 20:30:57'),
	(113, 'alex21', '?????', 38, '2019/08/13 20:31:06'),
	(114, 'pesho33', 'what?', 36, '2019/08/14 11:26:36'),
	(115, 'Icko', 'Hello', 39, '2019/08/14 19:10:29'),
	(116, 'Icko', 'How are You?', 39, '2019/08/14 19:10:49'),
	(117, 'Icko', 'Is this course good?', 39, '2019/08/14 19:11:22'),
	(118, 'Icko', 'This course is amazing', 39, '2019/08/14 19:11:50'),
	(119, 'Icko', 'comment123', 37, '2019/08/14 19:47:47'),
	(120, 'Icko', 'comment321', 37, '2019/08/14 19:47:57'),
	(121, 'Icko', 'comment3214', 37, '2019/08/14 19:48:16'),
	(122, 'Icko', 'comment3214', 37, '2019/08/14 19:48:46'),
	(123, 'Icko', '', 37, '2019/08/14 19:55:40'),
	(124, 'Icko', '', 37, '2019/08/14 19:56:08'),
	(125, 'alexdered', 'Join this course', 55, '2019/08/14 20:49:37'),
	(126, 'alexdered', 'It is free ', 55, '2019/08/14 20:49:55'),
	(127, 'alexdered', 'Robotics is an interdisciplinary branch of engineering and science that includes mechanical engineering', 55, '2019/08/14 20:50:28'),
	(128, 'alexdered', 'Join this course', 55, '2019/08/14 20:50:43'),
	(129, 'alexdered', 'It is free', 55, '2019/08/14 20:50:53'),
	(130, 'alexdered', 'Robotics is an interdisciplinary branch of engineering', 55, '2019/08/14 20:51:27');
/*!40000 ALTER TABLE `comments` ENABLE KEYS */;

-- Дъмп структура за таблица teacherapp.course
CREATE TABLE IF NOT EXISTS `course` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` char(50) NOT NULL,
  `topicID` int(11) NOT NULL,
  `description` varchar(200) NOT NULL,
  `teacher_username` varchar(50) NOT NULL,
  `picture_name` varchar(50) NOT NULL,
  `isDeleted` bit(1) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_course_topic` (`topicID`),
  CONSTRAINT `FK_course_topic` FOREIGN KEY (`topicID`) REFERENCES `topic` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=56 DEFAULT CHARSET=latin1;

-- Дъмп данни за таблица teacherapp.course: ~12 rows (approximately)
/*!40000 ALTER TABLE `course` DISABLE KEYS */;
INSERT INTO `course` (`id`, `title`, `topicID`, `description`, `teacher_username`, `picture_name`, `isDeleted`) VALUES
	(36, 'Digital Marketing', 10, 'Overseeing the social media strategy for the company. Managing online brand and product campaigns to raise brand awareness.', 'alex21', '/uploads/nathan-dumlao-LTDuGUByp6Y-unsplash.jpg', b'0'),
	(37, 'Soccer', 9, 'The sport of soccer (called football in most of the world) is considered to be the world\'s most popular sport. In soccer there are two teams of eleven players.', 'alex21', '/uploads/vikram-tkv-JO19K0HDDXI-unsplash.jpg', b'0'),
	(38, 'Geometry', 11, 'Geometry is a branch of mathematics concerned with questions of shape, size, relative position of figures, and the properties of space. ', 'Arnie', '/uploads/dawid-malecki-fw7lR3ibfpU-unsplash.jpg', b'0'),
	(39, 'Programming Basics', 12, 'Computer programmers write code to create software programs. They turn the program designs created by software developers and engineers into instructions that a computer can follow.', 'alex21', '/uploads/pankaj-patel-u2Ru4QBXA5Q-unsplash.jpg', b'0'),
	(46, 'sdsadsadasd', 8, 'fedfaewfewafwefefdcdsaceafdsfaewadssafeadddddddddddddddddesfffffffffffffffffffffffff', 'Arnie', '/uploads/user.png', b'1'),
	(47, '11111111111111111111111111111', 9, '11111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111', 'Arnie', '/uploads/user.png', b'1'),
	(48, 'History of Japan', 18, 'Over the following centuries the power of the Emperor and the imperial court gradually declined and passed to the military clans and their armies of samurai warriors.', 'donUser', '/uploads/clay-banks-hwLAI5lRhdM-unsplash.jpg', b'0'),
	(49, 'America Before Columbus', 18, 'History books traditionally depict the pre-Columbus Americas as a pristine wilderness where small native villages lived in harmony with nature.', 'donUser', '/uploads/photo-1510265382668-7b564935d7b5.jpg', b'0'),
	(50, 'Graphic Design', 19, 'Description', 'donUser', '/uploads/photo-1497091071254-cc9b2ba7c48a.jpg', b'0'),
	(53, 'Electrical Engineering', 7, 'Engineering is the use of scientific principles to design and build machines, structures, and other things, including bridges, roads, vehicles, and buildings.', 'alexdered', '/uploads/photo-1517373116369-9bdb8cdc9f62.jpg', b'0'),
	(54, 'Mechaical engineering', 7, 'Engineering is the use of scientific principles to design and build machines, structures, and other things, including bridges, roads, vehicles, and buildings.', 'alexdered', '/uploads/photo-1517524206127-48bbd363f3d7.jpg', b'0'),
	(55, 'Robotic', 7, 'Engineering is the use of scientific principles to design and build machines, structures, and other things, including bridges, roads, vehicles, and buildings.', 'alexdered', '/uploads/robotic.jpg', b'0');
/*!40000 ALTER TABLE `course` ENABLE KEYS */;

-- Дъмп структура за таблица teacherapp.homework
CREATE TABLE IF NOT EXISTS `homework` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `student_username` varchar(50) NOT NULL,
  `courseID` int(11) NOT NULL,
  `lectureID` int(11) NOT NULL,
  `teacher_username` varchar(50) NOT NULL,
  `homework_name` varchar(50) NOT NULL,
  `grade` int(11) NOT NULL DEFAULT 0,
  PRIMARY KEY (`id`),
  KEY `FK_homework_course` (`courseID`),
  KEY `FK_homework_lecture` (`lectureID`),
  KEY `FK_homework_users_info` (`student_username`),
  KEY `FK_homework_users_info_2` (`teacher_username`),
  CONSTRAINT `FK_homework_course` FOREIGN KEY (`courseID`) REFERENCES `course` (`id`),
  CONSTRAINT `FK_homework_lecture` FOREIGN KEY (`lectureID`) REFERENCES `lecture` (`id`),
  CONSTRAINT `FK_homework_users_info` FOREIGN KEY (`student_username`) REFERENCES `users_info` (`username`),
  CONSTRAINT `FK_homework_users_info_2` FOREIGN KEY (`teacher_username`) REFERENCES `users_info` (`username`)
) ENGINE=InnoDB AUTO_INCREMENT=66 DEFAULT CHARSET=latin1;

-- Дъмп данни за таблица teacherapp.homework: ~42 rows (approximately)
/*!40000 ALTER TABLE `homework` DISABLE KEYS */;
INSERT INTO `homework` (`id`, `student_username`, `courseID`, `lectureID`, `teacher_username`, `homework_name`, `grade`) VALUES
	(23, 'alex21', 36, 16, 'alex21', 'Java Questionnaire.docx', 77),
	(24, 'alex21', 36, 17, 'alex21', 'homework.txt', 66),
	(26, 'alex21', 36, 28, 'alex21', 'homework.txt', 88),
	(27, 'alex21', 38, 21, 'Arnie', 'homework.txt', 100),
	(28, 'Arnie', 39, 22, 'alex21', 'homework.txt', 80),
	(29, 'Arnie', 39, 23, 'alex21', 'homework.txt', 91),
	(30, 'Arnie', 39, 24, 'alex21', 'homework.txt', 92),
	(31, 'alexdered', 36, 16, 'alex21', 'homework.txt', 80),
	(32, 'alexdered', 36, 17, 'alex21', 'homework.txt', 85),
	(33, 'alexdered', 36, 28, 'alex21', 'homework.txt', 90),
	(34, 'alexdered', 38, 21, 'Arnie', 'Homework.docx', 88),
	(35, 'pant', 38, 21, 'Arnie', 'Homework.docx', 89),
	(36, 'pant1', 38, 21, 'Arnie', 'Homework.docx', 87),
	(37, 'pant2', 38, 21, 'Arnie', 'Homework.docx', 82),
	(38, 'pant3', 36, 16, 'alex21', 'Homework.docx', 99),
	(39, 'pant3', 36, 17, 'alex21', 'Homework.docx', 99),
	(40, 'pant3', 36, 28, 'alex21', 'Homework.docx', 99),
	(41, 'pant3', 38, 21, 'Arnie', 'Homework.docx', 99),
	(42, 'pant3', 37, 18, 'alex21', 'Homework.docx', 99),
	(43, 'pant3', 37, 19, 'alex21', 'Homework.docx', 99),
	(44, 'pant3', 37, 20, 'alex21', 'Homework.docx', 99),
	(45, 'pant4', 38, 21, 'Arnie', 'Homework.docx', 88),
	(46, 'pant4', 46, 29, 'Arnie', 'Homework.docx', 88),
	(47, 'pant4', 47, 30, 'Arnie', 'Homework.docx', 88),
	(48, 'pant5', 38, 21, 'Arnie', 'Homework.docx', 99),
	(49, 'pant5', 46, 29, 'Arnie', 'Homework.docx', 99),
	(50, 'pant5', 47, 30, 'Arnie', 'Homework.docx', 99),
	(51, 'pant6', 38, 21, 'Arnie', 'Homework.docx', 98),
	(52, 'pant6', 46, 29, 'Arnie', 'Homework.docx', 88),
	(53, 'pant6', 47, 30, 'Arnie', 'Homework.docx', 99),
	(54, 'pant7', 47, 30, 'Arnie', 'Homework.docx', 88),
	(55, 'pant7', 46, 29, 'Arnie', 'Homework.docx', 99),
	(56, 'pant7', 38, 21, 'Arnie', 'Homework.docx', 95),
	(57, 'pant7', 37, 18, 'alex21', 'Homework.docx', 88),
	(58, 'pant7', 37, 19, 'alex21', 'Homework.docx', 99),
	(59, 'pant7', 37, 20, 'alex21', 'Homework.docx', 99),
	(60, 'pesho33', 36, 16, 'alex21', 'homeworkLecture.docx', 88),
	(61, 'pesho33', 36, 17, 'alex21', 'homework.txt', 99),
	(62, 'pesho33', 36, 28, 'alex21', 'homework.txt', 99),
	(63, 'Icko', 37, 18, 'alex21', 'homeworkLecture.docx', 77),
	(64, 'Icko', 37, 19, 'alex21', 'homeworkLecture.docx', 80),
	(65, 'Icko', 37, 20, 'alex21', 'homeworkLecture.docx', 100);
/*!40000 ALTER TABLE `homework` ENABLE KEYS */;

-- Дъмп структура за таблица teacherapp.lecture
CREATE TABLE IF NOT EXISTS `lecture` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(50) NOT NULL,
  `description` varchar(1000) NOT NULL,
  `video_name` varchar(50) NOT NULL,
  `assignment_name` varchar(50) NOT NULL,
  `course_id` int(11) NOT NULL,
  `isDeleted` bit(1) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_lecture_course` (`course_id`),
  CONSTRAINT `FK_lecture_course` FOREIGN KEY (`course_id`) REFERENCES `course` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=42 DEFAULT CHARSET=latin1;

-- Дъмп данни за таблица teacherapp.lecture: ~21 rows (approximately)
/*!40000 ALTER TABLE `lecture` DISABLE KEYS */;
INSERT INTO `lecture` (`id`, `title`, `description`, `video_name`, `assignment_name`, `course_id`, `isDeleted`) VALUES
	(16, 'Digital Marketing 2019', 'Digital Marketing Trends for 2019 Online Domination', 'JJCGCuUIVzc', 'homework.txt', 36, b'0'),
	(17, 'Social Media', 'How To Start Social Media Marketing As A Beginner In 2019 - Step By Step Training', 'cA7dOKsd--I', 'TODO.txt', 36, b'0'),
	(18, 'Defending Skills', 'We are going over how to defend against fast attackers in soccer in this video!', 'hQIw-huztiY', 'TODO.txt', 37, b'0'),
	(19, 'Speed Lecture', 'Learn how to become faster with the football as we team up with the guys from Tanner Speed ', 'UHq765y9d0Y', 'TODO.txt', 37, b'0'),
	(20, 'Score More ', ' Learn how to score more goals on the pitch and learn how to shoot better. ', 'CxzwMe4TL74', 'TODO.txt', 37, b'0'),
	(21, 'Halves and fourths', 'Learn how to divide shapes into two or four equal sections.', '0lSTXtwPuOU', 'TODO.txt', 38, b'0'),
	(22, 'Java Tutorial for Beginners', 'Java tutorial for beginners - Learn Java, the language behind millions of apps and websites. ', 'eIrMbAQSU34', 'TODO.txt', 39, b'0'),
	(23, 'Python', 'Python Tutorial for Beginners ', '_uQrJ0TkZlc', 'TODO.txt', 39, b'0'),
	(24, 'JavaScript', 'JavaScript Tutorial for Beginners: Learn JavaScript in 1 Hour ', 'W6NZfCO5SIk', 'TODO.txt', 39, b'0'),
	(28, 'YouTube Marketing', 'Want to market your YouTube videos to the world? In this tutorial by PC Classes Online we\'ll show you all the techniques we\'ve learned to help promote your videos and your channel.', 'qbSGpTvYkSg', 'homework.txt', 36, b'0'),
	(29, 'titoddsadas', 'sadfdsafdsgfsssssssssssssssssssssssssssssa', 'fssFXlNk6vw', 'user.png', 46, b'0'),
	(30, '11111111111111111111111111111111', '111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111', '79wcQWWFyh8', 'user.png', 47, b'0'),
	(31, 'History of Japan\'s Ancient and Modern Empire', 'To the first westerners in Japan, it was a mysterious world.  It was the fabled isle of Zipangu that Marco Polo had only heard about.  The land of riches that Christopher Columbus set out to find.  ', 'qip8g_K4yTs', 'homework.txt', 48, b'0'),
	(32, 'America Before Columbus', 'When Columbus stepped ashore in 1492, millions of people were already living there.', 'whP9RL5huhE', 'homework.txt', 48, b'0'),
	(33, 'Introduction to graphic designing', 'Graphic design is the process of visual communication and problem-solving through the use of typography, photography, and illustration. ', '7cSSp8IW4vU', 'homework.txt', 50, b'0'),
	(34, ' Graphic Design Principles', ' Common uses of graphic design include corporate design (logos and branding) and editorial design (magazines, newspapers and books)', '5DRBzmoIhfI', 'homework.txt', 50, b'0'),
	(37, 'Introduction to Electrical Engineering', 'MIT 6.01SC Introduction to Electrical Engineering and Computer Science', '3S4cNfl0YF0', 'homework.txt', 53, b'0'),
	(38, 'Crash Course Engineering', 'Mechanical Engineering: Crash Course Engineering', 'A1V-QQ5wFU4', 'homework.txt', 54, b'0'),
	(39, 'Lecture 1 MIT Robotic', 'Robotics is an interdisciplinary branch of engineering and science that includes mechanical engineering, electronic engineering, information engineering, computer science, and others', 'oVaTKBlfPyA', 'homework.txt', 55, b'0'),
	(40, 'Next Generation Robots', 'Robotics deals with the design, construction, operation, and use of robots, as well as computer systems for their control, sensory feedback, and information processing.', '8vIT2da6N_o', 'homework.txt', 55, b'0'),
	(41, 'Boston Dynamics', 'These technologies are used to develop machines that can substitute for humans and replicate human actions.', 'dKjCWfuvYxQ', 'homework.txt', 55, b'0');
/*!40000 ALTER TABLE `lecture` ENABLE KEYS */;

-- Дъмп структура за таблица teacherapp.passed_lecture
CREATE TABLE IF NOT EXISTS `passed_lecture` (
  `student_username` varchar(50) NOT NULL,
  `course_id` int(11) NOT NULL,
  `lecture_id` int(11) NOT NULL,
  KEY `FK_passed_lecture_course` (`course_id`),
  KEY `FK_passed_lecture_lecture` (`lecture_id`),
  KEY `FK_passed_lecture_users_info` (`student_username`),
  CONSTRAINT `FK_passed_lecture_course` FOREIGN KEY (`course_id`) REFERENCES `course` (`id`),
  CONSTRAINT `FK_passed_lecture_lecture` FOREIGN KEY (`lecture_id`) REFERENCES `lecture` (`id`),
  CONSTRAINT `FK_passed_lecture_users_info` FOREIGN KEY (`student_username`) REFERENCES `users_info` (`username`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Дъмп данни за таблица teacherapp.passed_lecture: ~42 rows (approximately)
/*!40000 ALTER TABLE `passed_lecture` DISABLE KEYS */;
INSERT INTO `passed_lecture` (`student_username`, `course_id`, `lecture_id`) VALUES
	('alex21', 36, 16),
	('alex21', 36, 17),
	('alex21', 36, 28),
	('alex21', 38, 21),
	('Arnie', 39, 22),
	('Arnie', 39, 23),
	('Arnie', 39, 24),
	('alexdered', 36, 16),
	('alexdered', 36, 17),
	('alexdered', 36, 28),
	('alexdered', 38, 21),
	('pant', 38, 21),
	('pant1', 38, 21),
	('pant2', 38, 21),
	('pant3', 36, 16),
	('pant3', 36, 17),
	('pant3', 36, 28),
	('pant3', 38, 21),
	('pant3', 37, 18),
	('pant3', 37, 19),
	('pant3', 37, 20),
	('pant4', 38, 21),
	('pant4', 46, 29),
	('pant4', 47, 30),
	('pant5', 38, 21),
	('pant5', 46, 29),
	('pant5', 47, 30),
	('pant6', 38, 21),
	('pant6', 46, 29),
	('pant6', 47, 30),
	('pant7', 47, 30),
	('pant7', 46, 29),
	('pant7', 38, 21),
	('pant7', 37, 18),
	('pant7', 37, 19),
	('pant7', 37, 20),
	('pesho33', 36, 16),
	('pesho33', 36, 17),
	('pesho33', 36, 28),
	('Icko', 37, 18),
	('Icko', 37, 19),
	('Icko', 37, 20);
/*!40000 ALTER TABLE `passed_lecture` ENABLE KEYS */;

-- Дъмп структура за таблица teacherapp.rating
CREATE TABLE IF NOT EXISTS `rating` (
  `student_username` varchar(50) NOT NULL,
  `courseId` int(11) NOT NULL,
  `rating` int(11) NOT NULL,
  PRIMARY KEY (`student_username`,`courseId`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Дъмп данни за таблица teacherapp.rating: ~21 rows (approximately)
/*!40000 ALTER TABLE `rating` DISABLE KEYS */;
INSERT INTO `rating` (`student_username`, `courseId`, `rating`) VALUES
	('alex21', 38, 4),
	('alexdered', 36, 5),
	('alexdered', 38, 5),
	('Icko', 37, 4),
	('pant1', 38, 5),
	('pant2', 38, 5),
	('pant3', 36, 5),
	('pant3', 37, 5),
	('pant3', 38, 5),
	('pant4', 38, 5),
	('pant4', 46, 5),
	('pant4', 47, 5),
	('pant5', 38, 5),
	('pant5', 46, 5),
	('pant5', 47, 5),
	('pant6', 38, 4),
	('pant6', 46, 2),
	('pant6', 47, 4),
	('pant7', 37, 1),
	('pant7', 46, 3),
	('pesho33', 36, 4);
/*!40000 ALTER TABLE `rating` ENABLE KEYS */;

-- Дъмп структура за таблица teacherapp.status
CREATE TABLE IF NOT EXISTS `status` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `status` varchar(50) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

-- Дъмп данни за таблица teacherapp.status: ~2 rows (approximately)
/*!40000 ALTER TABLE `status` DISABLE KEYS */;
INSERT INTO `status` (`id`, `status`) VALUES
	(1, 'ongoing'),
	(2, 'completed');
/*!40000 ALTER TABLE `status` ENABLE KEYS */;

-- Дъмп структура за таблица teacherapp.student_course
CREATE TABLE IF NOT EXISTS `student_course` (
  `student_username` varchar(50) NOT NULL,
  `courseID` int(11) NOT NULL,
  `avg_grade` double NOT NULL,
  `statusID` int(11) NOT NULL,
  PRIMARY KEY (`student_username`,`courseID`),
  KEY `studentID` (`student_username`),
  KEY `FK_student_course_stauts` (`statusID`),
  CONSTRAINT `FK_student_course_stauts` FOREIGN KEY (`statusID`) REFERENCES `status` (`id`),
  CONSTRAINT `FK_student_course_users_info` FOREIGN KEY (`student_username`) REFERENCES `users_info` (`username`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Дъмп данни за таблица teacherapp.student_course: ~31 rows (approximately)
/*!40000 ALTER TABLE `student_course` DISABLE KEYS */;
INSERT INTO `student_course` (`student_username`, `courseID`, `avg_grade`, `statusID`) VALUES
	('alex21', 36, 77, 2),
	('alex21', 37, 0, 1),
	('alex21', 38, 100, 2),
	('alexdered', 36, 85, 2),
	('alexdered', 37, 0, 1),
	('alexdered', 38, 88, 1),
	('Arnie', 39, 87, 2),
	('Icko', 37, 85, 2),
	('Icko', 39, 0, 1),
	('pant', 38, 89, 1),
	('pant1', 38, 87, 2),
	('pant2', 38, 82, 2),
	('pant3', 36, 99, 2),
	('pant3', 37, 99, 2),
	('pant3', 38, 99, 2),
	('pant4', 38, 88, 2),
	('pant4', 46, 88, 2),
	('pant4', 47, 88, 2),
	('pant5', 38, 99, 2),
	('pant5', 46, 99, 2),
	('pant5', 47, 99, 2),
	('pant6', 38, 98, 2),
	('pant6', 46, 88, 2),
	('pant6', 47, 99, 2),
	('pant7', 36, 0, 1),
	('pant7', 37, 95, 2),
	('pant7', 38, 95, 2),
	('pant7', 39, 0, 1),
	('pant7', 46, 99, 2),
	('pant7', 47, 88, 2),
	('pesho33', 36, 95, 2);
/*!40000 ALTER TABLE `student_course` ENABLE KEYS */;

-- Дъмп структура за таблица teacherapp.topic
CREATE TABLE IF NOT EXISTS `topic` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=20 DEFAULT CHARSET=latin1;

-- Дъмп данни за таблица teacherapp.topic: ~13 rows (approximately)
/*!40000 ALTER TABLE `topic` DISABLE KEYS */;
INSERT INTO `topic` (`id`, `name`) VALUES
	(7, 'Engineering'),
	(8, 'New Topic'),
	(9, 'Sports'),
	(10, 'Marketing'),
	(11, 'Early Math '),
	(12, 'Programming'),
	(13, 'French'),
	(14, 'NewTopic777'),
	(15, 'javascript'),
	(16, 'Alex'),
	(17, 'Java'),
	(18, 'History'),
	(19, 'Design');
/*!40000 ALTER TABLE `topic` ENABLE KEYS */;

-- Дъмп структура за таблица teacherapp.users
CREATE TABLE IF NOT EXISTS `users` (
  `username` varchar(50) NOT NULL,
  `password` varchar(68) NOT NULL,
  `enabled` tinytext NOT NULL,
  PRIMARY KEY (`username`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Дъмп данни за таблица teacherapp.users: ~25 rows (approximately)
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` (`username`, `password`, `enabled`) VALUES
	('aleks33', '{bcrypt}$2a$10$QZA6H3JbPWGsgcjCcOqCWuXCYywwvBfwl5vfFGFqBPQeaaSSMKRPG', '1'),
	('alex144', '{bcrypt}$2a$10$QZIM4KJLxZzffUZx2lG8i.K.4qeewLd4gq1ehTT487OmlKllsYlEO', '1'),
	('alex21', '{bcrypt}$2a$10$o3bLHlkTwqVnzliBOqtnbOxFdNL1Ma12NJnH0gbaojYM5dYrSXqte', '1'),
	('alex22', '{bcrypt}$2a$10$HATpWJHGshDMBmRCSP54ZOf1YjYL5LSaD4Dvn.1cC9DmRDXUJHIDm', '1'),
	('alexdered', '{bcrypt}$2a$10$0dC4M9W6hYrizF2hTLth1eTUR/.CA5HjFt6MFOEYBKK.NmorL9hcq', '1'),
	('Arnie', '{bcrypt}$2a$10$/EWa2lG3W2QWHbUdPM/zfeSO30Xlb4X2mp4NGDRIkgILYpfepwXc.', '1'),
	('didig', '{bcrypt}$2a$10$bXCKJ55VP5UkGWqYXCTofesB3.bkPd0sz7eWhpiB1jxOntIRjRK6m', '1'),
	('donUser', '{bcrypt}$2a$10$5uPyM12kcAfymUaO5RMGF.Ccm/aoCfSvVd5mznzKYej0Djx2f7JL.', '1'),
	('Icko', '{bcrypt}$2a$10$vITAXcnPWI.PaW0y/R22jeunz0hgnSaV38RyMg8Hde8ZwD6p5qbBW', '1'),
	('ivan85', '{bcrypt}$2a$10$1HF0/tV2HGwpTI7CKbSxCuG6jq./UEp607Nd4ePVHWgIgwYV2.wne', '1'),
	('MartinM', '{bcrypt}$2a$10$l3XFeJ/Y5CfFN/7IOmUdoO1MN27OHrYiDSWorfbt43rD0kH24RcvO', '1'),
	('pant', '{bcrypt}$2a$10$1fe7LxIVn5qa.HD2llygdOvjUv07qJWO9tcNvqFnF76NvHlIoASEq', '1'),
	('pant1', '{bcrypt}$2a$10$FKja6ki4wjIXAdBarTUZJu5dmrr8p/PD/jw18dYoMq03wfqsClCby', '1'),
	('pant2', '{bcrypt}$2a$10$vA/paw29gGuFj7zWRt.oxuAUthSmuRHByNnCVCs0X4IBSh1044m/2', '1'),
	('pant3', '{bcrypt}$2a$10$jeMFPNiQ7EeBpaJyJi2MPe7FDSsrXTFpRLTXajUBOZEMy01r5eof6', '1'),
	('pant4', '{bcrypt}$2a$10$MHn6At3m7JP7nsMQeCsRKes3byDRNW4aKDp.GPlUsP5Sa1iY0vksW', '1'),
	('pant5', '{bcrypt}$2a$10$GWIa0VF7xhTWgouGm0v7EelBak3pg8cvG/T6IChqnzI/zXZtJRaOS', '1'),
	('pant6', '{bcrypt}$2a$10$xWjBXUEkHt.JwSv7QrdmS.JV/MM4L1spUuBrlx/F88v5JiJsC8mYS', '1'),
	('pant7', '{bcrypt}$2a$10$hRWw0l1AjBbYd2UxfWlkse24ZtMFPW7ptGQh9BJKJoI1FlKfy2XOq', '1'),
	('pant8', '{bcrypt}$2a$10$HtzezVrVziDl.PBapmdnoOhkcMVameIO6jUOqrJYeYvfuVpOgk5Ui', '1'),
	('pant9', '{bcrypt}$2a$10$JpxxeRo/Ay/LsQ1NAo7rcOY8abaeR0vuby34KHz4ew7rjD2m8ezUC', '1'),
	('pantwww', '{bcrypt}$2a$10$/MFJXw7xmwLoe8dWaNY5VOxk71xHbFDWu7.ik8kpd7yH4SAE00rdm', '1'),
	('pesho33', '{bcrypt}$2a$10$tG5VKaovT3eGTxdLXH2aKunuyW..mopaKCeMZ/dY6vjxtlmdEHx66', '1'),
	('SisiG', '{bcrypt}$2a$10$R.R7JO10fkxk0.C7RbN3N.1R2MCEV9WcgywGFXYENGbvZIr6m2lJi', '1'),
	('teach', '{bcrypt}$2a$10$Dt6RWA7ShpaITzSIN6pGUO0pv6fBAI4pO56irJRX3954rs4HWj01S', '1');
/*!40000 ALTER TABLE `users` ENABLE KEYS */;

-- Дъмп структура за таблица teacherapp.users_info
CREATE TABLE IF NOT EXISTS `users_info` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `firstname` varchar(50) NOT NULL,
  `lastname` varchar(50) NOT NULL,
  `username` varchar(50) NOT NULL,
  `email` varchar(50) NOT NULL,
  `picture_Name` varchar(50) NOT NULL,
  `isDeleted` bit(1) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_users_info_users` (`username`),
  CONSTRAINT `FK_users_info_users` FOREIGN KEY (`username`) REFERENCES `users` (`username`)
) ENGINE=InnoDB AUTO_INCREMENT=67 DEFAULT CHARSET=latin1;

-- Дъмп данни за таблица teacherapp.users_info: ~24 rows (approximately)
/*!40000 ALTER TABLE `users_info` DISABLE KEYS */;
INSERT INTO `users_info` (`id`, `firstname`, `lastname`, `username`, `email`, `picture_Name`, `isDeleted`) VALUES
	(34, 'Aleks', 'Kamarski', 'alex21', 'aleks.kamarski@gmail.com', 'cat1.jpg', b'0'),
	(42, 'Arnold', 'Schwarzenegger', 'Arnie', 'arnold.schwarzenegger@gmail.com', 'kim.jpg', b'0'),
	(45, 'Alex', 'Dered', 'alexdered', 'alexdered@hotmail.com', 'photo-1542744094-24638eff58bb.jpg', b'0'),
	(46, 'panter', 'pant', 'pant', 'clover88bg@yahoo.com', 'user.png', b'0'),
	(47, 'Alexandar', 'Deredjian', 'pant1', 'alexdered@hotmail.com', 'user.png', b'0'),
	(48, 'Alexandar', 'Deredjian', 'pant2', 'alexdered@hotmail.com', 'user.png', b'0'),
	(49, 'Alexandar', 'Deredjian', 'pant3', 'alexdered@hotmail.com', 'user.png', b'0'),
	(50, 'Alexandar', 'Deredjian', 'pant4', 'alexdered@hotmail.com', 'user.png', b'0'),
	(51, 'Alexandar', 'Deredjian', 'pant5', 'alexdered@hotmail.com', 'user.png', b'0'),
	(52, 'Alexandar', 'Deredjian', 'pant6', 'alexdered@hotmail.com', 'user.png', b'0'),
	(53, 'Alex', 'Deredjian', 'pant7', 'alexdered@hotmail.com', 'pant.jpg', b'0'),
	(54, 'Alexandar', 'Deredjian', 'teach', 'alexdered@hotmail.com', 'kim.jpg', b'0'),
	(55, 'pant8', 'pant8', 'pant8', 'alexdered@hotmail.com', 'monkey.jpg', b'0'),
	(56, 'Alexandar', 'Deredjian', 'pant9', 'alexdered@hotmail.com', 'user.png', b'0'),
	(57, 'Alex', 'Kamarski', 'aleks33', 'aleks.kamarski@gmail.com', 'user.png', b'0'),
	(58, 'Alexandar', 'Deredjian', 'alex144', 'alexdered@hotmail.com', 'user.png', b'0'),
	(59, 'Alexandar', 'Deredjian', 'pantwww', 'alexdered@hotmail.com', 'user.png', b'0'),
	(60, 'Martin', 'Milanov', 'MartinM', 'marto22@mail.bg', 'joshua-earle-4UexhWLVcnY-unsplash.jpg', b'0'),
	(61, 'Didi', 'Gospodinova', 'didig', 'diana_gospodinowa@abv.bg', 'cat.jpg', b'0'),
	(62, 'Don', 'Vasilev', 'donUser', 'don@mail.bg', 'joshua-earle-4UexhWLVcnY-unsplash.jpg', b'0'),
	(63, 'Silvia', 'Georgieva', 'SisiG', 'sisi77@gmail.com', 'joshua-earle-4UexhWLVcnY-unsplash.jpg', b'0'),
	(64, 'Pesho', 'Petrov', 'pesho33', 'todor@telerikacademy.com', 'joshua-earle-4UexhWLVcnY-unsplash.jpg', b'0'),
	(65, 'Ivan', 'Shishman', 'ivan85', 'ivan8523@abv.bg', 'monkey.jpg', b'0'),
	(66, 'Hristo', 'Asparuhov', 'Icko', 'ico.a@mail.bg', 'photo-1485814082842-a30d1628c44d.jpg', b'0');
/*!40000 ALTER TABLE `users_info` ENABLE KEYS */;

-- Дъмп структура за таблица teacherapp.user_request
CREATE TABLE IF NOT EXISTS `user_request` (
  `username` varchar(50) NOT NULL,
  `request_type` bit(1) NOT NULL,
  `isActive` bit(1) NOT NULL,
  PRIMARY KEY (`username`,`request_type`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Дъмп данни за таблица teacherapp.user_request: ~8 rows (approximately)
/*!40000 ALTER TABLE `user_request` DISABLE KEYS */;
INSERT INTO `user_request` (`username`, `request_type`, `isActive`) VALUES
	('alex21', b'0', b'0'),
	('alexdered', b'0', b'0'),
	('didig', b'0', b'0'),
	('donUser', b'0', b'0'),
	('Icko', b'0', b'1'),
	('MartinM', b'0', b'0'),
	('pesho33', b'0', b'1'),
	('SisiG', b'0', b'0');
/*!40000 ALTER TABLE `user_request` ENABLE KEYS */;

/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
