package com.example.teacherapp.models;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(name = "student_course")
public class StudentCourse implements Serializable {
    @Id
    @Column(name= "student_username")
    private String studentUsername;

    @Id
    @Column(name="courseID")
    private int courseID;

    @Column(name="avg_grade")
    private int avgGrade;

    @Column(name="statusID")
    private int statusID;


    public StudentCourse() {

    }

    public StudentCourse(String studentUsername, int courseID, int avgGrade, int statusID) {
        this.studentUsername = studentUsername;
        this.courseID = courseID;
        this.avgGrade = 0;
        this.statusID = 1;
    }


    public String getStudentUsername() {
        return studentUsername;
    }

    public int getCourseID() {
        return courseID;
    }

    public int getAvgGrade() {
        return avgGrade;
    }

    public int getStatusID() {
        return statusID;
    }

    public void setStudentUsername(String studentID) {
        this.studentUsername = studentID;
    }

    public void setCourseID(int courseID) {
        this.courseID = courseID;
    }

    public void setAvgGrade(int avgGrade) {
        this.avgGrade = avgGrade;
    }

    public void setStatusID(int statusID) {
        this.statusID = statusID;
    }
}
