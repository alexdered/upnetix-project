package com.example.teacherapp.models;

import javax.persistence.*;
import javax.validation.constraints.Size;

@Entity
@Table(name = "topic")
public class Topic {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private int id;

    @Size(min = 1 , max = 20 , message = "Topic size is not in the correct format")
    @Column(name = "name")
    private String name;

    public Topic() {

    }

    public Topic(@Size(min = 1, max = 20, message = "Topic size is not in the correct format") String name) {
        this.name = name;
    }

    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public void setId(int id) {
        this.id = id;
    }

    public void setName(String name) {
        this.name = name;
    }
}