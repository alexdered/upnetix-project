package com.example.teacherapp.models;

import javax.validation.constraints.Size;

public class CourseDTO {

    @Size(min = 1 , max = 50 , message = "Title is not in the correct size")
    private String text;

    public CourseDTO() {
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }
}
