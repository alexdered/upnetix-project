package com.example.teacherapp.models;

import javax.persistence.*;
import javax.validation.constraints.Size;
import java.util.Objects;

@Entity
@Table(name = "homework")
public class Homework implements Comparable {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private int id;

    @Column(name = "student_username")
    private String studentUsername;

    @Column(name = "courseID")
    private int courseId;

    @Column(name = "lectureID")
    private int lectureId;

    @Column(name = "teacher_username")
    private String teacherUsername;

    @Size(min = 1 , max = 50 , message = "Homework size is not in the correct format")
    @Column(name = "homework_name")
    private String homeworkName;

    @Column(name = "grade")
    private int grade;

    public Homework() {

    }

    public Homework(String studentUsername, int courseId, int lectureId, String teacherUsername, String homeworkName) {
        this.studentUsername = studentUsername;
        this.courseId = courseId;
        this.lectureId = lectureId;
        this.teacherUsername = teacherUsername;
        this.homeworkName = homeworkName;
        this.grade = 0;
    }


    public int getId() {
        return id;
    }

    public String getStudentUsername() {
        return studentUsername;
    }

    public int getCourseId() {
        return courseId;
    }

    public int getLectureId() {
        return lectureId;
    }

    public String getTeacherUsername() {
        return teacherUsername;
    }

    public String getHomeworkName() {
        return homeworkName;
    }

    public int getGrade() {
        return grade;
    }


    public void setId(int id) {
        this.id = id;
    }

    public void setStudentUsername(String studentUsername) {
        this.studentUsername = studentUsername;
    }

    public void setCourseId(int courseId) {
        this.courseId = courseId;
    }

    public void setLectureId(int lectureId) {
        this.lectureId = lectureId;
    }

    public void setTeacherUsername(String teacherUsername) {
        this.teacherUsername = teacherUsername;
    }

    public void setHomeworkName(String homeworkName) {
        this.homeworkName = homeworkName;
    }

    public void setGrade(int grade) {
        this.grade = grade;
    }

    @Override
    public int hashCode() {
        return Objects.hash(studentUsername, courseId);
    }

    @Override
    public boolean equals(Object obj) {
        Homework homework = (Homework) obj;
        return studentUsername.equals(homework.getStudentUsername()) && courseId==homework.getCourseId();
    }


    @Override
    public int compareTo(Object o) {
        Homework homework =(Homework) o;

        return courseId - homework.courseId;
    }
}
