package com.example.teacherapp.models;

import org.springframework.stereotype.Component;

import javax.persistence.*;
import javax.validation.constraints.Size;
import java.util.Objects;


@Entity
@Table(name= "course")
public class Course implements Comparable {


    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private int id;

    @Size(min = 1 , max = 50 , message = "Title size is not in the correct format")
    @Column(name = "title")
    private String title;


    @Column(name = "topicId")
    private int topicId;

    @Size(min = 1 , max = 200 , message = "Description size is not in the correct format")
    @Column(name = "description")
    private String description;


    @Column(name = "teacher_username")
    private String teacherUsername;

    @Column(name = "picture_name")
    private String pictureName;

    @Column(name = "isDeleted")
    private boolean isDeleted;

    public Course() {

    }

    public Course(String title, int topicId, String description, String teacherUsername, String pictureName) {
        this.title = title;
        this.topicId = topicId;
        this.description = description;
        this.teacherUsername = teacherUsername;
        this.pictureName = pictureName;
        this.isDeleted = false;
    }

    public int getId() {
        return id;
    }

    public String getTitle() {
        return title;
    }

    public int getTopicId() {
        return topicId;
    }

    public String getDescription() {
        return description;
    }

    public String getTeacherUsername() {
        return teacherUsername;
    }

    public String getPictureName() {
        return pictureName;
    }

    public boolean isDeleted() {
        return isDeleted;
    }

    public void setId(int id) {
        this.id = id;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public void setTopicId(int topicId) {
        this.topicId = topicId;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public void setTeacherUsername(String teacherUsername) {
        this.teacherUsername = teacherUsername;
    }

    public void setPictureName(String pictureName) {
        this.pictureName = pictureName;
    }

    public void setDeleted(boolean deleted) {
        isDeleted = deleted;
    }

    @Override
    public int hashCode() {
        return Objects.hash(title);
    }

    @Override
    public boolean equals(Object obj) {
        Course course = (Course) obj;
        return title.equals(course.title);
    }

    @Override
    public int compareTo(Object o) {
        Course course= (Course) o;
        return title.compareTo(course.getTitle());
    }
}
