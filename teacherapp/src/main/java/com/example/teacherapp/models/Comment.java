package com.example.teacherapp.models;

import javax.persistence.*;

@Entity
@Table(name="comments")
public class Comment {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private int id;

    @Column(name = "username")
    private String username;

    @Column(name = "comment")
    private String comment;

    @Column(name = "course_id")
    private int courseId;

    @Column(name = "time")
    private String time;

    public Comment() {
    }

    public Comment(String username, String comment, int courseId, String time) {
        this.username = username;
        this.comment = comment;
        this.courseId = courseId;
        this.time = time;
    }

    public int getId() {
        return id;
    }

    public String getUsername() {
        return username;
    }

    public String getComment() {
        return comment;
    }

    public int getCourseId() {
        return courseId;
    }

    public String getTime() {
        return time;
    }

    public void setId(int id) {
        this.id = id;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public void setCourseId(int courseId) {
        this.courseId = courseId;
    }

    public void setTime(String time) {
        this.time = time;
    }
}
