package com.example.teacherapp.models;
import javax.validation.constraints.Size;

public class UserDTO {
    @Size(min = 2 , max = 20 , message = "First name should be between 2 and 20 characters")
    private String firstName;

    @Size(min = 2 , max = 20 , message = "Last name should be between 2 and 20 characters")
    private String lastName;

    @Size(min = 10 , max = 50 , message = "Email should be between 10 and 50 characters")
    private String email;

    @Size(min = 2 , max = 20 , message = "Username should be between 2 and 20 characters")
    private String username;

    @Size(min = 2 , max = 20 , message = "Password should be between 2 and 20 characters")
    private String password;

    @Size(min = 2 , max = 20 , message = "Password should be between 2 and 20 characters")
    private String rePassword;

    public UserDTO() {

    }

    public String getFirstName() {
        return firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public String getEmail() {
        return email;
    }

    public String getUsername() {
        return username;
    }

    public String getPassword() {
        return password;
    }

    public String getRePassword() {
        return rePassword;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public void setRePassword(String rePassword) {
        this.rePassword = rePassword;
    }
}