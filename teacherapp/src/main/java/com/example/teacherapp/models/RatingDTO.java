package com.example.teacherapp.models;

public class RatingDTO {
    private int rating;

    public RatingDTO() {
    }

    public int getRating() {
        return rating;
    }

    public void setRating(int rating) {
        this.rating = rating;
    }
}
