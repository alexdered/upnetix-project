package com.example.teacherapp.models;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(name = "passed_lecture")
public class PassedLecture implements Serializable {

    @Id
    @Column(name = "student_username")
    private String studentName;

    @Id
    @Column(name = "course_id")
    private int courseId;

    @Id
    @Column(name = "lecture_id")
    private int lectureId;

    public PassedLecture() {

    }

    public PassedLecture(String studentName, int courseId, int lectureId) {
        this.studentName = studentName;
        this.courseId = courseId;
        this.lectureId = lectureId;
    }

    public String getStudentName() {
        return studentName;
    }

    public int getCourseId() {
        return courseId;
    }

    public int getLectureId() {
        return lectureId;
    }

    public void setStudentName(String studentId) {
        this.studentName = studentId;
    }

    public void setCourseId(int courseId) {
        this.courseId = courseId;
    }

    public void setLectureId(int lectureId) {
        this.lectureId = lectureId;
    }

}
