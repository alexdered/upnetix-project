package com.example.teacherapp.models;
import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;
import javax.validation.constraints.Size;
import java.util.List;
import java.util.Objects;

@Entity
@Table(name = "lecture")
public class Lecture {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private int id;

    @Column(name = "course_id")
    private int courseId;

    @Size(min = 1 , max = 50 , message = "Titile size is not in the correct format")
    @Column(name = "title")
    private String title;

    @Size(min = 1 , max = 1000 , message = "Description size is not in the correct format")
    @Column(name = "description")
    private String description;

    @Size(min = 1 , max = 200 , message = "Video size is not in the correct format")
    @Column(name = "video_name")
    private String videoName;

    @Size(min = 1 , max = 50 , message = "Assignment size is not in the correct format")
    @Column(name = "assignment_name")
    private String assignmentName;

    @Column(name = "isDeleted")
    private boolean isDeleted;

    public Lecture() {

    }

    public Lecture(int courseId, String title, String description, String videoName, String assignmentName) {
        this.courseId = courseId;
        this.title = title;
        this.description = description;
        this.videoName = videoName;
        this.assignmentName = assignmentName;
        this.isDeleted = false;
    }

    public int getId() {
        return id;
    }

    public String getTitle() {
        return title;
    }

    public String getDescription() {
        return description;
    }

    public String getVideoName() {
        return videoName;
    }

    public String getAssignmentName() {
        return assignmentName;
    }

    public int getCourseId() {
        return courseId;
    }

    public boolean isDeleted() {
        return isDeleted;
    }

    public void setId(int id) {
        this.id = id;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public void setVideoName(String videoName) {
        this.videoName = videoName;
    }

    public void setAssignmentName(String assignmentNmae) {
        this.assignmentName = assignmentNmae;
    }

    public void setCourseId(int courseId) {
        this.courseId = courseId;
    }

    public void setDeleted(boolean deleted) {
        isDeleted = deleted;
    }

    @Override
    public int hashCode() {
        return Objects.hash(title);
    }

    @Override
    public boolean equals(Object obj) {
        Lecture lecture = (Lecture) obj;

        return title.equals(lecture.getTitle());
    }
}
