package com.example.teacherapp.models;


import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;

@Entity
@Table(name = "user_request")
public class UserRequest implements Serializable {

    @Id
    @Column(name = "username")
    private String username;

    @Id
    @Column (name = "request_type")
    private int requestType;

    @Column(name="isActive")
    private boolean isActive;

    public UserRequest() {

    }

    public UserRequest(String username, int requestType) {
        this.username = username;
        this.requestType = requestType;
        this.isActive = true;
    }


    public String getUsername() {
        return username;
    }

    public int getRequestType() {
        return requestType;
    }

    public boolean isActive() {
        return isActive;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public void setRequestType(int requestType) {
        this.requestType = requestType;
    }

    public void setActive(boolean active) {
        isActive = active;
    }
}
