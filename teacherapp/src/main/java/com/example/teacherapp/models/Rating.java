package com.example.teacherapp.models;


import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(name = "rating")
public class Rating implements Serializable {

    @Id
    @Column (name = "student_username")
    private String studentUsername;

    @Id
    @Column(name = "courseId")
    private int courseId;

    @Column(name = "rating")
    private int rating;

    public Rating() {
    }


    public Rating(String studentUsername, int courseId, int rating) {
        this.studentUsername = studentUsername;
        this.courseId = courseId;
        this.rating = rating;
    }

    public String getStudentUsername() {
        return studentUsername;
    }

    public int getCourseId() {
        return courseId;
    }

    public int getRating() {
        return rating;
    }

    public void setStudentUsername(String studentUsername) {
        this.studentUsername = studentUsername;
    }

    public void setCourseId(int courseId) {
        this.courseId = courseId;
    }

    public void setRating(int grade) {
        this.rating = grade;
    }
}
