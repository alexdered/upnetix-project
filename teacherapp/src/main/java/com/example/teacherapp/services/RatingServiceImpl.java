package com.example.teacherapp.services;

import com.example.teacherapp.models.Rating;
import com.example.teacherapp.repositories.RatingRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.*;
import java.util.stream.Collectors;

import static java.util.stream.Collectors.toMap;

@Service
public class RatingServiceImpl implements RatingService {
    private RatingRepo ratingRepo;

    @Autowired
    public RatingServiceImpl(RatingRepo ratingRepo) {
        this.ratingRepo = ratingRepo;
    }

    @Override
    public Rating createRatingForCourse(String studentUsername, int courseId, int rating) throws IllegalArgumentException {
        if (rating > 5 || rating <= 0) {
            throw new IllegalArgumentException("Rating can be between 1 and 5 stars");
        }

        List<Rating> checkForExistingRating = ratingRepo.getByCourseAndStudent(studentUsername, courseId);

        if (checkForExistingRating.isEmpty()) {

            Rating newRating = new Rating(studentUsername, courseId, rating);

            return ratingRepo.createRatingForCourse(newRating);
        }
        return null;
    }

    @Override
    public List<Rating> getAll() {
        return ratingRepo.getAll();
    }

    @Override
    public List<Rating> getByCourseAndStudent(String studentUsername, int courseId) {
        return ratingRepo.getByCourseAndStudent(studentUsername, courseId);
    }

    @Override
    public Double getAverageCourseRating(int courseID) {
        List<Rating> courseRatingList = ratingRepo.getAll().stream().filter(p -> p.getCourseId() == courseID).collect(Collectors.toList());
        Double averageCourseRating;
        Double sum = 0.0;
        for (Rating i : courseRatingList) {
            sum = sum + i.getRating();
        }

        averageCourseRating = sum / Double.valueOf(courseRatingList.size());

        try {

            String temp = averageCourseRating.toString();

            if (temp.length() > 4) {
                temp = temp.substring(0, 4);
            }
            averageCourseRating = Double.valueOf(temp);
            if (temp == "NaN") {
                averageCourseRating = 0.0;
            }


        } catch (Exception ex) {
            averageCourseRating = 0.0;
        }
        return averageCourseRating;
    }

    @Override
    public List<Integer> getAllRatedCoursesWithAvgRating() {

        List<Rating> all = ratingRepo.getAll();
        HashMap<Integer, Double> uniqueCourseIdWithAvgRating = new HashMap<>();
        TreeMap<Double, Integer> orderedRatingsForCourses = new TreeMap<>();
        List<Integer> orderedCourseId = new ArrayList<>();

        for (Rating rating : all) {

            int courseId = rating.getCourseId();

            List<Rating> ratingListForCourse = ratingRepo.getAll().stream()
                    .filter(rating1 -> rating1.getCourseId() == courseId).collect(Collectors.toList());

            Double averageCourseRating;
            double sum = 0.0;
            for (Rating i : ratingListForCourse) {
                sum = sum + i.getRating();
            }

            averageCourseRating = sum / (double) ratingListForCourse.size();

            if (!uniqueCourseIdWithAvgRating.containsKey(courseId)) {
                uniqueCourseIdWithAvgRating.put(courseId, averageCourseRating);
            }

        }


        HashMap<Integer, Double> sorted = uniqueCourseIdWithAvgRating
                .entrySet()
                .stream()
                .sorted(Collections.reverseOrder(Map.Entry.comparingByValue()))
                .collect(
                        toMap(Map.Entry::getKey, Map.Entry::getValue, (e1, e2) -> e2,
                                LinkedHashMap::new));


        sorted.forEach((key, value) -> orderedCourseId.add(key));

//        uniqueCourseIdWithAvgRating.forEach((key, value) -> orderedRatingsForCourses.put(value, key));
//
//        orderedRatingsForCourses.forEach((key, value) -> orderedCourseId.add(value));
//
//        Collections.reverse(orderedCourseId);

        return orderedCourseId;
    }
}
