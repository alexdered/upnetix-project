package com.example.teacherapp.services;

import com.example.teacherapp.models.Homework;

import java.util.List;

public interface HomeworkService {

    Homework create(String studentUsername, int courseId, int lectureId,
                    String teacherUsername, String homeworkName , int grade);

    Homework getById(int homeworkId);

    List<Homework> getAllForStudent(String studentUsername);

    List<Homework> getAllForTeacher( String teacherUsername);

    boolean existStudentByLecture(String studentUsername, int lectureId);

    Homework update(Homework homework);

    int grade(int grade, int id);

}
