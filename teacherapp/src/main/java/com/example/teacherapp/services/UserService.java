package com.example.teacherapp.services;

import com.example.teacherapp.models.User;

import java.util.List;

public interface UserService {

    User create(User user);

    List<User> getAll();

    User updateUser(User user);

    User getSessionUser();

    List<User> getAdminPretenders();

    User delete(User user);

    User getByUsername(String username);

    List<User> getAllTeachers();

    List<User> getAllStudents();
}
