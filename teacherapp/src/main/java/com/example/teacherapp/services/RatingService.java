package com.example.teacherapp.services;


import com.example.teacherapp.models.Rating;

import javax.persistence.criteria.CriteriaBuilder;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;

public interface RatingService {

    Rating createRatingForCourse(String studentUsername, int courseId, int grade) throws IllegalArgumentException;

    List<Rating> getAll();

    List<Rating> getByCourseAndStudent(String studentUsername, int courseId);

    Double getAverageCourseRating(int courseID);

    List<Integer> getAllRatedCoursesWithAvgRating();

}
