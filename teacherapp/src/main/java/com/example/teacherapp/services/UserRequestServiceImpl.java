package com.example.teacherapp.services;

import com.example.teacherapp.models.UserRequest;
import com.example.teacherapp.repositories.UserRequestRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
@Transactional
public class UserRequestServiceImpl implements UserRequesService {
    private UserRequestRepo userRequestRepo;

    @Autowired
    public UserRequestServiceImpl(UserRequestRepo userRequestRepo) {
        this.userRequestRepo = userRequestRepo;
    }

    @Override
    public UserRequest create(String username, int requestType) {

        UserRequest checkUserRequest = userRequestRepo.getByUserAndRequest(username, requestType);

        if (checkUserRequest == null) {
            UserRequest newUserRequest = new UserRequest(username, requestType);
            userRequestRepo.create(newUserRequest);
            return newUserRequest;
        }

        return null;
    }

    @Override
    public List<UserRequest> getAllTeacherRequests() {
        return userRequestRepo.getAllTeacherRequests();
    }

    @Override
    public List<UserRequest> getAllAdminRequests() {
        return userRequestRepo.getAllAdminRequests();
    }

    @Override
    public UserRequest getByUserAndRequest(String username, int requestType) {
        return userRequestRepo.getByUserAndRequest(username, requestType);
    }

    @Override
    public UserRequest approveRequest(String username, int requestType) {

        UserRequest userRequestToBeUpdated = userRequestRepo.getByUserAndRequest(username, requestType);
        userRequestToBeUpdated.setActive(false);

        UserRequest updatedRequest = userRequestRepo.approveRequest(userRequestToBeUpdated);

        return updatedRequest;
    }
}
