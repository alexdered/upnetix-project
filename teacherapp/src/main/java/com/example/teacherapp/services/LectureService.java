package com.example.teacherapp.services;

import com.example.teacherapp.models.Lecture;

import java.util.List;


public interface LectureService {

    Lecture create(String title, String description, String videoName , String assignmentName, int CourseId);

    List<Lecture> getAllL();

    List<Lecture> getAllForCourse(int courseId);

    Lecture getById(int id);

}
