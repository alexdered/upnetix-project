package com.example.teacherapp.services;

import com.example.teacherapp.models.Comment;
import com.example.teacherapp.repositories.CommentRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;

@Service
public class CommentServiceImpl implements CommentService{

    private CommentRepo commentRepo;
    private UserService userService;

    @Autowired
    public CommentServiceImpl(CommentRepo commentRepo, UserService userService) {
        this.commentRepo = commentRepo;
        this.userService = userService;
    }

    @Override
    public Comment createComment(String commentFromUser, int courseId) {

        String noJsonComment="";
        String[] noJsonArray = commentFromUser.split("");
        for (int i = 9; i <noJsonArray.length ; i++) {
            if(noJsonArray[i].equals("\"")){break;}
            noJsonComment=noJsonComment+noJsonArray[i];
        }

        DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyy/MM/dd HH:mm:ss");
        LocalDateTime now = LocalDateTime.now();
        String time = dtf.format(now);
        String username = userService.getSessionUser().getUsername();

        Comment comment = new Comment(username ,noJsonComment, courseId, time);

        return commentRepo.createComment(comment);
    }

    @Override
    public List<Comment> getAllComments() {

        List<Comment> comments = commentRepo.getAllComments();
//        List<String> onlyComments = new ArrayList<>();
//
//        for (Comment i:comments){
//            onlyComments.add(i.getComment());
//        }

        return comments;
    }
}
