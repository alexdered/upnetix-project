package com.example.teacherapp.services.functional;

public interface CourseTracker {

    boolean courseStatusValidator(int homewrokId);

    void totalGradeEvaluator(boolean checkIfCourseStatusIsFinished, int homeworkId);

}
