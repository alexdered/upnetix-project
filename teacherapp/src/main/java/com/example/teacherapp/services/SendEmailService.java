package com.example.teacherapp.services;

public interface SendEmailService {

    void sendConfirmationEmail(String userEmailAddress);
}
