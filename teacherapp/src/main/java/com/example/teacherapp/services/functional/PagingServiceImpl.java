package com.example.teacherapp.services.functional;

import com.example.teacherapp.models.Course;
import com.example.teacherapp.repositories.CourseRepo;
import com.example.teacherapp.services.RatingService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class PagingServiceImpl implements Pageable {

    private CourseRepo courseRepo;
    private RatingService ratingService;

    @Autowired
    public PagingServiceImpl(CourseRepo courseRepo, RatingService ratingService) {
        this.courseRepo = courseRepo;
        this.ratingService = ratingService;
    }

    public List<Course> getAllActiveCourses(int page, int nextPage) {

        int max = 9;

        int first;
        if (nextPage == 0) {
            first = page * max - max;
        } else {
            first = (page + 1) * max - max;
        }
        List<Course> currentPageCourses = courseRepo.getAllActive(first, max);


        if (nextPage == 1) {

            int firstI = (page + 1) * max - max;

            List<Course> checkNextPage = courseRepo.getAllActive(firstI, max);

            if (!checkNextPage.isEmpty()) {
                return checkNextPage;
            }


        } else if (nextPage == -1 && page > 1) {
            int firstI = (page - 1) * max - max;

            List<Course> checkNextPage = courseRepo.getAllActive(firstI, max);

            if (!checkNextPage.isEmpty()) {
                return checkNextPage;
            }

        }

        return currentPageCourses;

    }

    public List<Course> getCoursesOrderedByTitle(int page, int nextPage) {

        int max = 9;

        int first;
        if (nextPage == 0) {
            first = page * max - max;
        } else {
            first = (page + 1) * max - max;
        }
        List<Course> currentPageCourses = courseRepo.orderedByName(first, max);


        if (nextPage == 1) {

            int firstI = (page + 1) * max - max;

            List<Course> checkNextPage = courseRepo.orderedByName(firstI, max);

            if (!checkNextPage.isEmpty()) {
                return checkNextPage;
            }


        } else if (nextPage == -1 && page > 1) {
            int firstI = (page - 1) * max - max;

            List<Course> checkNextPage = courseRepo.orderedByName(firstI, max);

            if (!checkNextPage.isEmpty()) {
                return checkNextPage;
            }

        }

        return currentPageCourses;

    }


}
