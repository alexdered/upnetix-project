package com.example.teacherapp.services.functional;

import org.springframework.stereotype.Service;

@Service
public class YouTubeIdGeneratorImpl implements YouTubeIdGenerator {

    public static final String THERE_IS_A_PROBLEM_WITH_THE_YOUTUBE_LINK = "There is a problem with the YouTube link";

    @Override
    public String generateId(String videoLink) {
        StringBuilder generatedId = new StringBuilder();
        Boolean isGenarating = false;

        if (!videoLink.contains("www.youtube.com")){
            throw new IllegalArgumentException(THERE_IS_A_PROBLEM_WITH_THE_YOUTUBE_LINK);
        }

            for (int i = 0; i < videoLink.length(); i++) {
                char ch = videoLink.charAt(i);

                if (isGenarating) {
                    generatedId.append(ch);
                }
                if (ch == '=') {
                    isGenarating = true;
                }
                if (generatedId.length() == 11) {
                    break;
                }
            }

            String generatedString = generatedId.toString();

            if (generatedString.length()!=11){
                throw new IllegalArgumentException(THERE_IS_A_PROBLEM_WITH_THE_YOUTUBE_LINK);
            }

            return generatedString ;

    }
}
