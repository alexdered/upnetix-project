package com.example.teacherapp.services.functional;

import com.example.teacherapp.services.HomeworkService;
import com.example.teacherapp.services.LectureService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.stream.Collectors;

@Service
public class ProgressTrackerImpl implements ProgressTracker {
    private HomeworkService homeworkService;
    private LectureService lectureService;

    @Autowired
    public ProgressTrackerImpl(HomeworkService homeworkService, LectureService lectureService) {
        this.homeworkService = homeworkService;
        this.lectureService = lectureService;
    }

    @Override
    public int totalProgress(String studentUsername, int courseId) {
        int x = (int) homeworkService.getAllForStudent(studentUsername).stream().filter(homework -> homework.getCourseId() == courseId).count();
        int y = lectureService.getAllForCourse(courseId).size();
        return (int)((double)x/y*100);
    }
}
