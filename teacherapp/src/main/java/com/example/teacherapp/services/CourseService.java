package com.example.teacherapp.services;

import com.example.teacherapp.models.Course;
import com.example.teacherapp.models.Topic;

import javax.mail.search.SearchTerm;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public interface CourseService {

    List<Topic> createCourseView();

    Course create(String title, String description, String teacherUsername, int topicId, String pictureName);

    List<Course> getAll();

    List<Course> getAllActivePaged(int page, int nextPage);

    List<Course> getAllActive();

    Course getById(int id);

    Course delete(int id);

    List<Course> getAllActiveByTeacher(String teacherUsername);

    List<Course> getAllOrderedByTitle(int page, int nextPage);

    List<Course> getAllOrderedByRating();

    List getTopThree();

    Set<Course> getAllFilteredByTitle(String title);

}
