package com.example.teacherapp.services;

import com.example.teacherapp.models.Course;
import com.example.teacherapp.models.Topic;
import com.example.teacherapp.repositories.CourseRepo;
import com.example.teacherapp.services.functional.Pageable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;


@Service
public class CourseServiceImpl implements CourseService {

    private CourseRepo courseRepo;
    private UserService userService;
    private Pageable pageable;
    private RatingService ratingService;

    @Autowired
    public CourseServiceImpl(CourseRepo courseRepo, UserService userService, Pageable pageable, RatingService ratingService) {
        this.courseRepo = courseRepo;
        this.userService = userService;
        this.pageable = pageable;
        this.ratingService = ratingService;
    }

    @Override
    public List<Topic> createCourseView() {
        List<Topic> topicsObj = courseRepo.createCourseView();
        return topicsObj;
    }

    @Override
    public Course create(String title, String description, String teacherUsername, int topicId, String pictureName) {

        List<Course> checkIfExist = courseRepo.checkIfExists(title);

        if (!checkIfExist.isEmpty()) {
            throw new IllegalArgumentException("Course has already been created!");
        }
        Course course = new Course();
        course.setTitle(title);
        course.setDescription(description);
        course.setTeacherUsername(teacherUsername);
        course.setDeleted(false);
        course.setTopicId(topicId);
        course.setPictureName(pictureName);

        return courseRepo.create(course);

    }

    @Override
    public List<Course> getAll() {
        return courseRepo.getAll();
    }

    @Override
    public List<Course> getAllActive() {
        List<Course> courses = getAll().stream().filter(course -> !course.isDeleted())
                .collect(Collectors.toList());

        return courses;
    }

    @Override
    public List<Course> getAllActivePaged(int page, int nextPage) {
        return pageable.getAllActiveCourses(page, nextPage);
    }

    @Override
    public Course getById(int id) {
        return courseRepo.getById(id);
    }

    @Override
    public Course delete(int id) {
        String teacherUsername = userService.getSessionUser().getUsername();
        Course course = getAllActive().stream().filter(course1 -> course1.getId() == id).filter(course1 -> course1.getTeacherUsername().equals(teacherUsername))
                .findAny().orElseThrow(() -> new IllegalArgumentException("Course not found"));
        course.setDeleted(true);
        return courseRepo.update(course);
    }


    @Override
    public List<Course> getAllActiveByTeacher(String teacherUsername) {
        return courseRepo.getByTeacher(teacherUsername).stream().filter(course -> !course.isDeleted()).collect(Collectors.toList());
    }

    @Override
    public List<Course> getAllOrderedByTitle(int page, int nextPage) {

        return pageable.getCoursesOrderedByTitle(page, nextPage);
    }

    @Override
    public List<Course> getAllOrderedByRating() {

        List<Integer> courseIdList = ratingService.getAllRatedCoursesWithAvgRating();

        List<Course> allActive = courseRepo.getAll();

        List<Course> coursesOrderedByRating = new ArrayList<>();

        for (Integer courseId : courseIdList) {

            for (Course course : allActive) {
                if (course.getId() == courseId) {
                    coursesOrderedByRating.add(course);

                }
            }
        }
        return coursesOrderedByRating;
    }

    @Override
    public List<Course> getTopThree() {
        List <Course> allCourses =  getAllOrderedByRating();

        List topThree = new ArrayList();

        for (int i = 0; i <3 ; i++) {
            topThree.add(allCourses.get(i));
        }

        return topThree;
    }

    @Override
    public Set<Course> getAllFilteredByTitle(String title) {

       title= title.toLowerCase();

        if (title.contains("'or'")){
            return null;
        }

        List<Course> coursesFilteredByTitle = courseRepo.filterByCourseName(title);


        List<Course> coursesFilteredByTeacher = courseRepo.filterByTeacher(title);

        List<Topic> topicObj = courseRepo.createCourseView();
        int topicId = 0;
        for (Topic topic : topicObj) {
            if (topic.getName().toLowerCase().equals(title)){
                topicId= topic.getId();
            }
        }

        List<Course> coursesFilteredByTopic = new ArrayList<>();

        List<Course> allCourses = courseRepo.getAll();
        for (Course crs : allCourses) {
            if (crs.getTopicId()==topicId){
                coursesFilteredByTopic.add(crs);
            }
        }

        Set<Course> allCoursesFound = new HashSet<>();
        allCoursesFound.addAll(coursesFilteredByTitle);
        allCoursesFound.addAll(coursesFilteredByTeacher);
        allCoursesFound.addAll(coursesFilteredByTopic);

        return allCoursesFound;
    }
}
