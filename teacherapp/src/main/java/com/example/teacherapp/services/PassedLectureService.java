package com.example.teacherapp.services;

import com.example.teacherapp.models.PassedLecture;

import java.util.List;

public interface PassedLectureService {

    PassedLecture create(String studentName, int courseId, int lectureId);

    List<PassedLecture> getAll();

    PassedLecture checkIfExist(String studentName ,int lectureId);
}
