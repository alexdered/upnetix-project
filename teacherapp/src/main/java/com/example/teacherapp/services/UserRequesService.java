package com.example.teacherapp.services;

import com.example.teacherapp.models.UserRequest;

import java.util.List;

public interface UserRequesService {

    UserRequest create(String username, int requestType);

    List<UserRequest> getAllTeacherRequests();

    List<UserRequest> getAllAdminRequests();

    UserRequest getByUserAndRequest(String username, int requestType);

    UserRequest approveRequest(String username, int requestType);

}
