package com.example.teacherapp.services;

import com.example.teacherapp.models.Course;
import com.example.teacherapp.models.StudentCourse;

import java.util.List;

public interface StudentCourseService {


    StudentCourse create(String studentUsername, int courseId);

    List<StudentCourse> getAll();

    List<Course> getAllActiveOngoingForStudent(String studentUsername);

    List<Course> getAllActiveFinishedForStudent(String studentUsername);

    StudentCourse getById(String studentUsername, int courseId);

    StudentCourse saveStatus(StudentCourse studentCourse, int status);

    StudentCourse saveTotalGrade(StudentCourse studentCourse, int totalGrade);


}
