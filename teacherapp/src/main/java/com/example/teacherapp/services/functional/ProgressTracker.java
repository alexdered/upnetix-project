package com.example.teacherapp.services.functional;

public interface ProgressTracker {
    int totalProgress(String studentUsername, int courseId);
}
