package com.example.teacherapp.services;

import com.example.teacherapp.models.Course;
import com.example.teacherapp.models.StudentCourse;
import com.example.teacherapp.repositories.StudentCourseRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class StudentCourseServiceImpl implements StudentCourseService {
    private StudentCourseRepo studentCourseRepo;
    private CourseService courseService;

    @Autowired
    public StudentCourseServiceImpl(StudentCourseRepo studentCourseRepo, CourseService courseService) {
        this.studentCourseRepo = studentCourseRepo;
        this.courseService = courseService;
    }

    @Override
    public StudentCourse create(String studentUsername, int courseId) {

        if (!studentCourseRepo.getById(studentUsername, courseId).isEmpty()) {
            throw new IllegalArgumentException("The student has enrolled for the course already");
        }
        StudentCourse studentCourse = new StudentCourse(studentUsername, courseId, 0, 1);
        return studentCourseRepo.create(studentCourse);
    }

    @Override
    public List<StudentCourse> getAll() {
        return studentCourseRepo.getAll();
    }

    @Override
    public List<Course> getAllActiveOngoingForStudent(String studentName) {

       List<StudentCourse> allStudentCourses = studentCourseRepo.getAll().stream()
               .filter(studentCourse -> studentCourse.getStudentUsername().equals(studentName)).collect(Collectors.toList());

       List<Course> activeStudentCourses = new ArrayList<>();

       List<Course> allActive = courseService.getAllActive();

        for (StudentCourse studentCourse : allStudentCourses) {
            for (Course active : allActive) {
                if (active.getId()== studentCourse.getCourseID()){
                    if (studentCourse.getStatusID()== 1)
                    activeStudentCourses.add(active);
                }
            }
        }

        return activeStudentCourses;
    }

    @Override
    public List<Course> getAllActiveFinishedForStudent(String studentName) {

        List<StudentCourse> allStudentCourses = studentCourseRepo.getAll().stream()
                .filter(studentCourse -> studentCourse.getStudentUsername().equals(studentName)).collect(Collectors.toList());

        List<Course> activeStudentCourses = new ArrayList<>();

        List<Course> allActive = courseService.getAllActive();

        for (StudentCourse studentCourse : allStudentCourses) {
            for (Course active : allActive) {
                if (active.getId()== studentCourse.getCourseID()){
                    if (studentCourse.getStatusID()== 2)
                        activeStudentCourses.add(active);
                }
            }
        }

        return activeStudentCourses;
    }

    @Override
    public StudentCourse getById(String studentUsername, int courseId) {
        List<StudentCourse> list = studentCourseRepo.getById(studentUsername, courseId);
        if (list.isEmpty()) {
            return null;
        }
        return list.get(0);
    }

    @Override
    public StudentCourse saveStatus(StudentCourse studentCourse, int status) {
        return studentCourseRepo.saveStatus(studentCourse,status);
    }

    @Override
    public StudentCourse saveTotalGrade(StudentCourse studentCourse, int grade) {
        return studentCourseRepo.saveTotalGrade(studentCourse, grade);
    }

}
