package com.example.teacherapp.services.functional;

import com.example.teacherapp.models.Homework;
import com.example.teacherapp.models.StudentCourse;
import com.example.teacherapp.services.HomeworkService;
import com.example.teacherapp.services.LectureService;
import com.example.teacherapp.services.StudentCourseService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class CourseTrackerImpl implements CourseTracker {

    private HomeworkService homeworkService;
    private LectureService lectureService;
    private StudentCourseService studentCourseService;

    @Autowired
    public CourseTrackerImpl(HomeworkService homeworkService, LectureService lectureService, StudentCourseService studentCourseService) {
        this.homeworkService = homeworkService;
        this.lectureService = lectureService;
        this.studentCourseService = studentCourseService;
    }

    @Override
    public boolean courseStatusValidator(int homeworkId) {

        Homework homework = homeworkService.getById(homeworkId);
        if (homework == null) {
            throw new IllegalArgumentException("Homework not found");
        }

        int homeworksCounted = (int) homeworkService.getAllForStudent(homework.getStudentUsername()).stream()
                .filter(homework1 -> homework1.getCourseId() == homework.getCourseId())
                .filter(homework1 -> homework1.getGrade() != 0).count();

        int lecturesCounted = lectureService.getAllForCourse(homework.getCourseId()).size();


        if (homeworksCounted == lecturesCounted) {
            StudentCourse studentCourse = studentCourseService.getById(homework.getStudentUsername(), homework.getCourseId());
            StudentCourse checkstudentCourse = studentCourseService.saveStatus(studentCourse, 2);
            return true;
        }
        return false;
    }

    @Override
    public void totalGradeEvaluator(boolean checkIfCourseStatusIsFinished, int homeworkId) {

        if (checkIfCourseStatusIsFinished) {
            Homework homework = homeworkService.getById(homeworkId);

            int homeworkSummed = homeworkService.getAllForStudent(homework.getStudentUsername()).stream()
                    .filter(homework1 -> homework1.getCourseId() == homework.getCourseId())
                    .filter(homework1 -> homework1.getGrade() != 0).mapToInt(Homework::getGrade).sum();

            int lecturesCounted = lectureService.getAllForCourse(homework.getCourseId()).size();

            int totalGrade = homeworkSummed / lecturesCounted;

            StudentCourse studentCourse = studentCourseService.getById(homework.getStudentUsername(), homework.getCourseId());
             studentCourseService.saveTotalGrade(studentCourse, totalGrade);

        }

    }
}
