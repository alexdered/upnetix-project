package com.example.teacherapp.services.functional;

import com.example.teacherapp.models.Course;

import java.util.List;

public interface Pageable {

    List<Course> getAllActiveCourses(int page, int nextPage);

    List<Course> getCoursesOrderedByTitle(int page, int nextPage);

}
