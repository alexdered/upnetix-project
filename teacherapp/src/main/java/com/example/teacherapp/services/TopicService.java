package com.example.teacherapp.services;

import com.example.teacherapp.models.Topic;

public interface TopicService {

    Topic saveTopic(Topic topic);
}
