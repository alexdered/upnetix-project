package com.example.teacherapp.services;

import com.example.teacherapp.models.Homework;
import com.example.teacherapp.repositories.HomeworkRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class HomeworkServiceImpl implements HomeworkService {
    private HomeworkRepo homeworkRepo;

    @Autowired
    public HomeworkServiceImpl(HomeworkRepo homeworkRepo) {
        this.homeworkRepo = homeworkRepo;
    }

    @Override
        public Homework create(String studentUsername, int courseId, int lectureId,
                               String teacherUsername, String homeworkName, int grade) {

        Homework homework = new Homework(studentUsername, courseId, lectureId, teacherUsername, homeworkName);

        return homeworkRepo.create(homework);
    }

    @Override
    public Homework getById(int homeworkId) {
        return homeworkRepo.getById(homeworkId);
    }

    @Override
    public List<Homework> getAllForStudent(String studentUsername) {
        return homeworkRepo.getAllForStudent(studentUsername);
    }

//    @Override
//    public List<Homework> getAllForTeacher(String teacherUsername) {
//        List<Homework> allHomeworks = homeworkRepo.getAll().stream().filter(homework -> homework.getTeacherUsername().equals(teacherUsername))
//                .collect(Collectors.toList());
//
//        return allHomeworks;
//    }

    @Override
    public List<Homework> getAllForTeacher(String teacherUsername) {

        return homeworkRepo.getAllForTeacher(teacherUsername);
    }


    @Override
    public boolean existStudentByLecture(String studentUsername, int lectureId) {
        List<Homework> homework = homeworkRepo.getForStudentByLecture(studentUsername,lectureId);

        return !homework.isEmpty();
    }

    @Override
    public Homework update(Homework homework) {
        return homeworkRepo.update(homework);
    }

    @Override
    public int grade(int grade, int id) {
        return homeworkRepo.grade(grade,id);
    }
}
