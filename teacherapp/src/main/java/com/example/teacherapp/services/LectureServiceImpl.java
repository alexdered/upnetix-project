package com.example.teacherapp.services;

import com.example.teacherapp.models.Lecture;
import com.example.teacherapp.repositories.LectureRepo;
import com.example.teacherapp.services.functional.YouTubeIdGenerator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class LectureServiceImpl implements LectureService {
    private LectureRepo lectureRepo;
    private YouTubeIdGenerator youTubeIdGenerator;

    @Autowired
    public LectureServiceImpl(LectureRepo lectureRepo, YouTubeIdGenerator youTubeIdGenerator) {
        this.lectureRepo = lectureRepo;
        this.youTubeIdGenerator = youTubeIdGenerator;
    }

    @Override
    public Lecture create(String title, String description, String videoName, String assignmentName, int courseId) {

        String videoId = youTubeIdGenerator.generateId(videoName);

        Lecture lecture = new Lecture(courseId, title, description, videoId, assignmentName);

        Lecture lectureShouldNotExist = getAllForCourse(courseId).stream().filter(lecture1 -> lecture1.equals(lecture))
                .findAny().orElse(null);

        if (lectureShouldNotExist != null) {
            throw new IllegalArgumentException("Lecture already exists");
        }
        return lectureRepo.create(lecture);
    }

    @Override
    public List<Lecture> getAllL() {
        return lectureRepo.getAll();
    }

    @Override
    public List<Lecture> getAllForCourse(int courseId) {
        List<Lecture> courseLectures = lectureRepo.getAll().stream().filter(lecture -> lecture.getCourseId() == courseId)
                .collect(Collectors.toList());

        return courseLectures;
    }


    @Override
    public Lecture getById(int id) {
        return lectureRepo.getById(id);

    }


}
