package com.example.teacherapp.services;


import com.example.teacherapp.models.Comment;

import java.util.List;

public interface CommentService {

    Comment createComment(String comment,int courseId);

    List<Comment> getAllComments();

}
