package com.example.teacherapp.services;

import com.example.teacherapp.models.User;
import com.example.teacherapp.repositories.UserRepo;
import com.example.teacherapp.repositories.UserRepoImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.provisioning.UserDetailsManager;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;

@Service
@Transactional
public class UserServiceImpl implements UserService {
    private UserRepo userRepo;
    private UserDetailsManager userDetailsManager;


    @Autowired
    public UserServiceImpl(UserRepo userRepo, UserDetailsManager userDetailsManager) {
        this.userRepo = userRepo;
        this.userDetailsManager = userDetailsManager;
    }

    @Override
    public User create(User user) {
        return userRepo.create(user);
    }

    @Override
    public List<User> getAll() {
        return userRepo.getAll();
    }

    @Override
    public User updateUser(User user) {

        User userUpdated = getSessionUser();
        userUpdated.setFirstName(user.getFirstName());
        userUpdated.setLastName(user.getLastName());
        userUpdated.setEmail(user.getEmail());
        if (!user.getPicture().equals("")) {
            userUpdated.setPicture(user.getPicture());
        }

        userRepo.updateUser(userUpdated);

        return user;
    }

    @Override
    public User getSessionUser() {

        Object principal = SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        String username = ((UserDetails) principal).getUsername();
        List<User> allUsers = userRepo.getAll();
        User sessionUser = allUsers.stream().filter(p -> p.getUsername().equals(username)).findAny().orElseThrow(IllegalArgumentException::new);

        return sessionUser;
    }


    @Override
    public List<User> getAdminPretenders() {
        List<User> users = userRepo.getAll();
        List<User> adminPretenders = new ArrayList<>();


        for (User user : users) {
            UserDetails userLoaded = userDetailsManager.loadUserByUsername(user.getUsername());
            if (userLoaded.getAuthorities().size() != 3) {
                adminPretenders.add(user);
            }
        }
        return adminPretenders;
    }


    @Override
    public User delete(User user) {
        user.setDeleted(true);
        return userRepo.updateUser(user);
    }

    @Override
    public User getByUsername(String username) {
        return userRepo.getByUsername(username);
    }

    @Override
    public List<User> getAllTeachers() {
        List<User> users = userRepo.getAll();
        List<User> teachers = new ArrayList<>();
        for (User user : users) {
            UserDetails userLoaded = userDetailsManager.loadUserByUsername(user.getUsername());
            if (userLoaded.getAuthorities().size() == 2) {
                teachers.add(user);
            }
        }

        return teachers;
    }

    @Override
    public List<User> getAllStudents() {
        List<User> users = userRepo.getAll();
        List<User> students = new ArrayList<>();
        for (User user : users) {
            UserDetails userLoaded = userDetailsManager.loadUserByUsername(user.getUsername());
            if (userLoaded.getAuthorities().size() == 1) {
                students.add(user);
            }
        }

        return students;
    }
}
