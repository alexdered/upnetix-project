package com.example.teacherapp.services;

import com.example.teacherapp.models.Topic;
import com.example.teacherapp.repositories.TopicRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class TopicServiceImpl implements TopicService {

    private TopicRepo topicRepo;

    @Autowired
    public TopicServiceImpl(TopicRepo topicRepo) {
        this.topicRepo = topicRepo;
    }

    @Override
    public Topic saveTopic(Topic topic) {
       topicRepo.saveTopic(topic);

       return topic;
    }
}
