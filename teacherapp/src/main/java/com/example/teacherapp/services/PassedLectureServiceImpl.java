package com.example.teacherapp.services;

import com.example.teacherapp.models.PassedLecture;
import com.example.teacherapp.repositories.PassedLectureRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class PassedLectureServiceImpl implements PassedLectureService {
    private PassedLectureRepo passedLectureRepo;

    @Autowired
    public PassedLectureServiceImpl(PassedLectureRepo passedLectureRepo) {
        this.passedLectureRepo = passedLectureRepo;
    }

    @Override
    public PassedLecture create(String studentName, int courseId, int lectureId) {
        PassedLecture passedLecture = new PassedLecture(studentName, courseId, lectureId);
       return passedLectureRepo.create(passedLecture);
    }

    @Override
    public List<PassedLecture> getAll() {
        return passedLectureRepo.getAll();
    }

    @Override
    public PassedLecture checkIfExist(String studentName, int lectureId) {
        List <PassedLecture> passedLecture= passedLectureRepo.checkIfExist(studentName,lectureId);
        if (passedLecture.isEmpty()){
            return null;
        }
        return passedLecture.get(0);
    }
}
