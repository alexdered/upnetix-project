package com.example.teacherapp.services.functional;

public interface YouTubeIdGenerator {

    String generateId(String link);

}
