package com.example.teacherapp.repositories;

import com.example.teacherapp.models.PassedLecture;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class PassedLectureRepoImpl implements PassedLectureRepo {
    private SessionFactory sessionFactory;

    @Autowired
    public PassedLectureRepoImpl(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    @Override
    public PassedLecture create(PassedLecture passedLecture) {
        try  (Session session = sessionFactory.openSession()) {
            Transaction tx = session.beginTransaction();
            session.save(passedLecture);
            tx.commit();
            session.close();
            return passedLecture;
        }
    }

    @Override
    public List<PassedLecture> getAll() {
        try  (Session session = sessionFactory.openSession()) {
            List<PassedLecture> passedLectures = session.createQuery("from PassedLecture", PassedLecture.class).list();
            return passedLectures;
        }
    }

    @Override
    public List<PassedLecture> checkIfExist(String studentName, int lectureId) {
        try (Session session = sessionFactory.openSession()) {
            return session.createQuery("select pl from PassedLecture pl where studentName like :studentUsername and lectureId = :lectureId")
                    .setParameter("studentUsername", studentName).setParameter("lectureId", lectureId).list();
        }
    }
}
