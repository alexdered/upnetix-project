package com.example.teacherapp.repositories;

import com.example.teacherapp.models.User;

import java.util.List;

public interface UserRepo {

    User create(User user);

    List<User> getAll();

    User updateUser(User user);

    User getByUsername(String username);
}
