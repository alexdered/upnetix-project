package com.example.teacherapp.repositories;

import com.example.teacherapp.models.PassedLecture;

import java.util.List;

public interface PassedLectureRepo {

    PassedLecture create(PassedLecture passedLecture);

    List<PassedLecture> getAll();

    List<PassedLecture> checkIfExist(String studentName, int lectureId);

}
