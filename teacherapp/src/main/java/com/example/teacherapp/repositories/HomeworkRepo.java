package com.example.teacherapp.repositories;

import com.example.teacherapp.models.Homework;

import java.util.List;

public interface HomeworkRepo {

    Homework create( Homework homework);

    List<Homework> getAll();

    Homework getById(int homeworkId);

    List<Homework> getAllForStudent(String studentUsername);

    List<Homework> getAllForTeacher(String teacherUsername);

    List<Homework> getForStudentByLecture(String studentUsername, int lectureId);

    Homework update(Homework homework);

    int grade(int grade, int id);

}
