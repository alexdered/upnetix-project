package com.example.teacherapp.repositories;

import com.example.teacherapp.models.Comment;
import com.example.teacherapp.models.Course;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;


@Repository
public class CommentRepoImpl implements CommentRepo{

    private SessionFactory sessionFactory;

    @Autowired
    public CommentRepoImpl(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    @Override
    public List<Comment> getAllComments() {

        try (Session session = sessionFactory.openSession()) {
            List<Comment> allComments = session.createQuery("from Comment", Comment.class).list();
            session.close();
            return allComments;
        }
    }

    @Override
    public Comment createComment(Comment comment) {

        try (Session session = sessionFactory.openSession()) {
            Transaction tx = session.beginTransaction();
            session.save(comment);
            tx.commit();
            session.close();
            return comment;
        }
    }


}
