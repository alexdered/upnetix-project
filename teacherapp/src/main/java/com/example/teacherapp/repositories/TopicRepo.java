package com.example.teacherapp.repositories;

import com.example.teacherapp.models.Topic;

public interface TopicRepo {

    Topic saveTopic(Topic topic);
}
