package com.example.teacherapp.repositories;

import com.example.teacherapp.models.Course;
import com.example.teacherapp.models.Topic;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;


@Repository
public class CourseRepoImpl implements CourseRepo {

    private SessionFactory sessionFactory;

    @Autowired
    public CourseRepoImpl(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }


    @Override
    public List<Topic> createCourseView() {
        try (Session session = sessionFactory.openSession()) {
            List<Topic> topicsObj = session.createQuery("\n" + "from Topic", Topic.class).list();
            return topicsObj;
        }
    }

    @Override
    public Course create(Course course) {
        try (Session session = sessionFactory.openSession()) {
            Transaction tx = session.beginTransaction();
            session.save(course);
            tx.commit();
            session.close();
            return course;
        }
    }

    @Override
    public List<Course> getAll() {
        try (Session session = sessionFactory.openSession()) {
            List<Course> allCourses = session.createQuery("from Course cr where cr.isDeleted=false", Course.class).list();
            session.close();
            return allCourses;
        }
    }


    @Override
    public List<Course> getAllActive(int first, int max) {
        try (Session session = sessionFactory.openSession()) {
            Query query = session.createQuery("select  cr from  Course  cr where cr.isDeleted = false", Course.class)
                    .setFirstResult(first)
                    .setMaxResults(max);
            List<Course> courses = query.list();

            session.close();
            return courses;
        }
    }

    @Override
    public Course getById(int id) {
        try (Session session = sessionFactory.openSession()) {
            List<Course> allActiveCourses = session.createQuery("select  cr from  Course  cr where cr.isDeleted = false and cr.id= :id", Course.class)
                    .setParameter("id", id).list();
            session.close();
            return allActiveCourses.get(0);
        }
    }

    @Override
    public Course update(Course course) {
        try (Session session = sessionFactory.openSession()) {
            Transaction tx = session.beginTransaction();
            session.update(course);
            tx.commit();
            session.close();
            return course;
        }
    }

    @Override
    public List<Course> checkIfExists(String title) {
        try (Session session = sessionFactory.openSession()) {
            Query<Course> courses = session.createQuery("select cr from Course cr where cr.title like :title", Course.class);
            courses.setParameter("title", title);

            return courses.list();
        }
    }

    @Override
    public List<Course> getByTeacher(String teacherUsername) {
        try (Session session = sessionFactory.openSession()) {
            Query<Course> courses = session.createQuery("select cr from Course cr where cr.teacherUsername like :teacher", Course.class);
            courses.setParameter("teacher", teacherUsername);

            return courses.list();
        }
    }


    @Override
    public List<Course> filterByCourseName(String courseName) {

        try (Session session = sessionFactory.openSession()) {
            Query<Course> courses = session.createQuery("select cr from Course cr where cr.isDeleted=false and cr.title like '%"+courseName+"%'", Course.class);

            return courses.list();
        }
    }

    @Override
    public List<Course> filterByTopic(int topicId) {
        try (Session session = sessionFactory.openSession()) {
            Query<Course> courses = session.createQuery("select cr from Course cr where cr.isDeleted=false and cr.topicId = :topicId", Course.class);
            courses.setParameter("topicId", topicId);

            return courses.list();
        }
    }

    @Override
    public List<Course> filterByTeacher(String teacherUsername) {
        try (Session session = sessionFactory.openSession()) {
            Query<Course> courses = session.createQuery("select cr from Course cr where cr.isDeleted=false and cr.teacherUsername like :teacher", Course.class);
            courses.setParameter("teacher", teacherUsername);

            return courses.list();
        }
    }

    @Override
    public List<Course> orderedByName(int first, int max) {
        try (Session session = sessionFactory.openSession()) {
            List<Course> allCourses = session.createQuery("from Course cr where cr.isDeleted = false order by cr.title", Course.class)
                    .setFirstResult(first)
                    .setMaxResults(max).list();
            session.close();
            return allCourses;
        }
    }
}
