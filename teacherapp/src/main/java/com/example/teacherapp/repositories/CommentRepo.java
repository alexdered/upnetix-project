package com.example.teacherapp.repositories;


import com.example.teacherapp.models.Comment;

import java.util.List;

public interface CommentRepo {

    List<Comment> getAllComments();

    Comment createComment(Comment comment);

}
