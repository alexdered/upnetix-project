package com.example.teacherapp.repositories;

import com.example.teacherapp.models.Rating;

import java.util.List;

public interface RatingRepo {

    Rating createRatingForCourse(Rating rating);

    List<Rating> getAll();

    List<Rating> getByCourseAndStudent(String studentUsername, int courseId);


}
