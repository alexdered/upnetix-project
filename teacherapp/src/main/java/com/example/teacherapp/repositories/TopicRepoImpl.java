package com.example.teacherapp.repositories;

import com.example.teacherapp.models.Topic;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.springframework.stereotype.Repository;

@Repository
public class TopicRepoImpl implements TopicRepo {

    private SessionFactory sessionFactory;

    public TopicRepoImpl(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }


    @Override
    public Topic saveTopic(Topic topic) {
        try (Session session = sessionFactory.openSession()) {
            Transaction tx = session.beginTransaction();

            session.save(topic);
            tx.commit();
            session.close();

            return topic;
        }
    }




}
