package com.example.teacherapp.repositories;

import com.example.teacherapp.models.Lecture;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class LectureRepoImpl implements LectureRepo{
   private SessionFactory sessionFactory;

   @Autowired
    public LectureRepoImpl(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    @Override
    public Lecture create(Lecture lecture) {
        try  (Session session = sessionFactory.openSession()) {
            Transaction tx = session.beginTransaction();
            session.save(lecture);
            tx.commit();
            session.close();

            return lecture;
        }
    }

    @Override
    public List<Lecture> getAll() {
        try  (Session session = sessionFactory.openSession()) {
            List<Lecture> allLectures = session.createQuery("from Lecture", Lecture.class).list();
            return allLectures;
        }

    }

    @Override
    public List<Lecture> getAllForCourse(int courseId) {
        return null;
    }

    @Override
    public Lecture getById(int id) {
       try (Session session= sessionFactory.openSession()){
           List<Lecture> lectures = session.createQuery("select lct from Lecture lct where lct.id = :id")
                   .setParameter("id", id).list();
           if (lectures.isEmpty()){
               return null;
           }

           return lectures.get(0);
       }
    }
}
