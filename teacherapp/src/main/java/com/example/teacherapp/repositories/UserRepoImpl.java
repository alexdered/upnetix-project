package com.example.teacherapp.repositories;

import com.example.teacherapp.models.User;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import javax.management.Query;
import java.util.List;

@Repository
public class UserRepoImpl implements UserRepo {

   private SessionFactory sessionFactory;

    @Autowired
    public UserRepoImpl(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    @Override
    public User create(User user) {
        Session  session = sessionFactory.getCurrentSession();
        try {
            session.save(user);
        } catch (Exception ex){
            throw new IllegalArgumentException("Something went wrong");
        }
        return user;
    }

    @Override
    public List<User> getAll() {

        try  (Session session = sessionFactory.openSession()) {
            Transaction tx = session.beginTransaction();
            List<User> allUsers = session.createQuery("select u from User u where u.isDeleted=false", User.class).list();
            tx.commit();
            session.close();
            return allUsers;
        }
    }

    @Override
    public User updateUser(User user) {

        try  (Session session = sessionFactory.openSession()) {
            Transaction tx = session.beginTransaction();
            session.update(user);
            tx.commit();
            session.close();
        }

        return user;
    }


    @Override
    public User getByUsername(String username) {
        try(Session session =sessionFactory.openSession() ){
            List<User> users = session.createQuery("select us from User us where us.username like :username", User.class)
                    .setParameter("username" , username).list();
            if (users.isEmpty()) {
                return null;
            }
            return users.get(0);
        }
    }
}
