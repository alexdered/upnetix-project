package com.example.teacherapp.repositories;


import com.example.teacherapp.models.Rating;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class RatingRepoImpl implements RatingRepo {
    SessionFactory sessionFactory;

    @Autowired
    public RatingRepoImpl(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    @Override
    public Rating createRatingForCourse(Rating rating) {
        try  (Session session = sessionFactory.openSession()) {
            Transaction tx = session.beginTransaction();
            session.save(rating);
            tx.commit();
            session.close();
            return rating;
        }
    }

    @Override
    public List<Rating> getAll() {
        try  (Session session = sessionFactory.openSession()) {
            List<Rating> allRatings = session.createQuery("from Rating ", Rating.class).list();
            session.close();
            return allRatings;
        }
    }

    @Override
    public List<Rating> getByCourseAndStudent(String studentUsername, int courseId) {
        try  (Session session = sessionFactory.openSession()) {
            Query<Rating> ratingQuery = session.createQuery("select rating from Rating rating where rating.studentUsername like :student and rating.courseId = :courseId ", Rating.class);
            ratingQuery.setParameter("student", studentUsername);
            ratingQuery.setParameter("courseId", courseId);

            return ratingQuery.list();
        }
    }

}
