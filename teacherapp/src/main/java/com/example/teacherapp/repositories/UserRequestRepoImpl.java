package com.example.teacherapp.repositories;

import com.example.teacherapp.models.UserRequest;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class UserRequestRepoImpl implements UserRequestRepo {
    private SessionFactory sessionFactory;

    @Autowired
    public UserRequestRepoImpl(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    @Override
    public UserRequest create(UserRequest userRequest) {
        try  (Session session = sessionFactory.openSession()) {
            Transaction tx = session.beginTransaction();
            session.save(userRequest);
            tx.commit();
            session.close();
            return userRequest;
        }
    }

    @Override
    public List<UserRequest> getAllTeacherRequests() {
        try  (Session session = sessionFactory.openSession()) {
            List<UserRequest> teacherRequests = session.createQuery("SELECT ur FROM UserRequest ur where ur.requestType=0 and ur.isActive = true ").list();
            return teacherRequests;
        }
    }

    @Override
    public List<UserRequest> getAllAdminRequests() {
        try  (Session session = sessionFactory.openSession()) {
            List<UserRequest> adminRequests = session.createQuery("SELECT ur FROM UserRequest ur where ur.requestType=1 and ur.isActive = true ").list();
            return adminRequests;
        }
    }

    @Override
    public UserRequest getByUserAndRequest(String username, int requestType) {
        try  (Session session = sessionFactory.openSession()) {
            List<UserRequest> currentRequestList = session.createQuery("SELECT ur FROM UserRequest ur where ur.requestType= :request and ur.username like :user and ur.isActive = true ")
                    .setParameter("request", requestType)
                    .setParameter("user", username)
                    .list();
            if (currentRequestList.isEmpty()) {
                return null;
            }
            UserRequest userRequest = currentRequestList.get(0);

            return userRequest;
        }
    }

    @Override
    public UserRequest approveRequest(UserRequest userRequest) {
        try  (Session session = sessionFactory.openSession()) {
            Transaction tx = session.beginTransaction();
            session.update(userRequest);
            tx.commit();
            session.close();

            return userRequest;
        }
    }
}
