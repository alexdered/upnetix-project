package com.example.teacherapp.repositories;

import com.example.teacherapp.models.StudentCourse;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.springframework.beans.factory.annotation.Autowired;
import org.hibernate.Session;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class StudentCourseRepoImpl implements StudentCourseRepo {
    private SessionFactory sessionFactory;

    @Autowired
    public StudentCourseRepoImpl(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    @Override
    public StudentCourse create(StudentCourse studentCourse) {
        try  (Session session = sessionFactory.openSession()) {
            Transaction tx = session.beginTransaction();
            session.save(studentCourse);
            tx.commit();
            session.close();
            return studentCourse;
        }

    }

    @Override
    public List<StudentCourse> getAll() {
        try  (Session session = sessionFactory.openSession()) {
            List<StudentCourse> listAll = session.createQuery("from StudentCourse ", StudentCourse.class).list();
            session.close();
            return listAll;
        }
    }

    @Override
    public List<StudentCourse> getById(String studentUsername, int courseId) {
        try  (Session session = sessionFactory.openSession()) {

            org.hibernate.query.Query<StudentCourse> studentCourses = session.createQuery("select sc from StudentCourse sc where studentUsername like :studentName and courseID = :cId", StudentCourse.class);
            studentCourses.setParameter("studentName", studentUsername);
            studentCourses.setParameter("cId", courseId);

            return studentCourses.list();
        }
    }


    @Override
    public StudentCourse saveStatus(StudentCourse studentCourse, int status) {
        try  (Session session = sessionFactory.openSession()) {
            Transaction tx = session.beginTransaction();
            studentCourse.setStatusID(status);
            session.delete(studentCourse);
            session.save(studentCourse);
            tx.commit();
            session.close();
            return studentCourse;
        }

    }

    @Override
    public StudentCourse saveTotalGrade(StudentCourse studentCourse, int grade) {
        try  (Session session = sessionFactory.openSession()) {
            Transaction tx = session.beginTransaction();
            studentCourse.setAvgGrade(grade);
            session.delete(studentCourse);
            session.save(studentCourse);
            tx.commit();
            session.close();
            return studentCourse;
        }
    }
}
