package com.example.teacherapp.repositories;

import com.example.teacherapp.models.Lecture;

import java.util.List;

public interface LectureRepo {

    Lecture create(Lecture lecture);

    List<Lecture> getAll();

    List<Lecture> getAllForCourse(int courseId);

    Lecture getById(int id);


}
