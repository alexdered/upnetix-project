package com.example.teacherapp.repositories;

import com.example.teacherapp.models.Homework;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class HomeworkRepoImpl implements HomeworkRepo {
    private SessionFactory sessionFactory;

    @Autowired
    public HomeworkRepoImpl(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    @Override
    public Homework create(Homework homework) {
        try (Session session = sessionFactory.openSession()) {
            Transaction tx = session.beginTransaction();
            session.save(homework);
            tx.commit();
            session.close();

            return homework;
        }

    }

    @Override
    public List<Homework> getAll() {
        try (Session session = sessionFactory.openSession()) {
            return session.createQuery("from Homework ", Homework.class).list();
        }
    }

    @Override
    public Homework getById(int homeworkId) {
        Session session = sessionFactory.openSession();
        return session.createQuery("from Homework where id = :id", Homework.class)
                .setParameter("id", homeworkId).getSingleResult();

    }

    @Override
    public List<Homework> getAllForStudent(String studentUsername) {

        try (Session session = sessionFactory.openSession()) {
            Query<Homework> homework = session.createQuery("select hw from Homework hw where hw.studentUsername like :username", Homework.class);
            homework.setParameter("username", studentUsername);
            return homework.list();
        }

    }

    @Override
    public List<Homework> getAllForTeacher(String teacherUsername) {
        try (Session session = sessionFactory.openSession()) {
            Query<Homework> homework = session.createQuery("select hw from Homework hw where hw.teacherUsername like :username", Homework.class);
            homework.setParameter("username", teacherUsername);
            return homework.list();
        }
    }

    @Override
    public List<Homework> getForStudentByLecture(String studentUsername, int lectureId) {
        try (Session session = sessionFactory.openSession()) {
            Query<Homework> homework = session.createQuery("select hw from Homework hw where hw.studentUsername like :username and hw.lectureId = :ID", Homework.class);
            homework.setParameter("username", studentUsername);
            homework.setParameter("ID", lectureId);
            return homework.list();
        }
    }

    @Override
    public Homework update(Homework homework) {
        try (Session session = sessionFactory.openSession()) {
            Transaction tx = session.beginTransaction();
            session.update(homework);
            tx.commit();
            session.close();
            return homework;
        }
    }

    @Override
    public int grade(int grade, int id) {
        Session session = sessionFactory.openSession();
        Transaction tx = session.beginTransaction();
        List<Homework> homeworks = getAll();
        Homework homework = homeworks.stream().filter(p -> p.getId() == id).findAny().orElseThrow(IllegalAccessError::new);
        homework.setId(id);
        homework.setGrade(grade);
        session.update(homework);
        tx.commit();
        session.close();
        return grade;
    }
}
