package com.example.teacherapp.repositories;

import com.example.teacherapp.models.UserRequest;

import java.util.List;

public interface UserRequestRepo {

    UserRequest create(UserRequest userRequest);

    List<UserRequest> getAllTeacherRequests();

    List<UserRequest> getAllAdminRequests();

    UserRequest getByUserAndRequest(String username, int requestType);

    UserRequest approveRequest(UserRequest userRequest);
}
