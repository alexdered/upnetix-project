package com.example.teacherapp.repositories;

import com.example.teacherapp.models.StudentCourse;

import java.util.List;

public interface StudentCourseRepo {

    StudentCourse create(StudentCourse studentCourseRepo);

    List<StudentCourse> getAll();

    List<StudentCourse> getById(String studentUsername, int courseId);

    StudentCourse saveStatus ( StudentCourse studentCourse, int status);

    StudentCourse saveTotalGrade(StudentCourse studentCourse, int grade);

}
