package com.example.teacherapp.repositories;

import com.example.teacherapp.models.Course;
import com.example.teacherapp.models.Topic;

import java.util.List;

public interface CourseRepo {

    List<Topic> createCourseView();

    Course create(Course course);

    List<Course> getAll();

    List<Course> getAllActive(int first , int max);

    Course getById(int id);

    Course update(Course course);

    List<Course> checkIfExists(String title);

    List<Course> getByTeacher(String TeacherUsername);

    List<Course> filterByCourseName(String courseName);

    List<Course> filterByTopic (int topicId);

    List<Course> filterByTeacher(String teacherUsername);

    List<Course> orderedByName(int first, int max);


}
