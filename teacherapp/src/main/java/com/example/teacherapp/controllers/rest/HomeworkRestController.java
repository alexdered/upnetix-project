package com.example.teacherapp.controllers.rest;

import com.example.teacherapp.models.Homework;
import com.example.teacherapp.services.HomeworkService;
import com.example.teacherapp.services.functional.CourseTracker;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import javax.validation.Valid;

@RestController
@RequestMapping("/api/homeworks")
public class HomeworkRestController {

    private HomeworkService homeworkService;
    private CourseTracker courseTracker;

    @Autowired
    public HomeworkRestController(HomeworkService homeworkService, CourseTracker courseTracker) {
        this.homeworkService = homeworkService;
        this.courseTracker = courseTracker;
    }

    @PostMapping("/grade")
    public int grade(@Valid @RequestParam(value = "grade") int grade,
                     @RequestParam(value = "id") int homeworkId) {

        try {
            int res = homeworkService.grade(grade, homeworkId);
            boolean bool = courseTracker.courseStatusValidator(homeworkId);
            courseTracker.totalGradeEvaluator(bool, homeworkId);
            return res;
        }catch (IllegalArgumentException ex) {
            throw new ResponseStatusException(HttpStatus.CONFLICT, ex.getMessage());
        }
    }

}




