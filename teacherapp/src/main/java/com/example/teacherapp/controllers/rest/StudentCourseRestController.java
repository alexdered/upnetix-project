package com.example.teacherapp.controllers.rest;

import com.example.teacherapp.models.StudentCourse;
import com.example.teacherapp.services.StudentCourseService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("/api/studentcourse")
public class StudentCourseRestController {
   private StudentCourseService studentCourseService;

   @Autowired
    public StudentCourseRestController(StudentCourseService studentCourseService) {
        this.studentCourseService = studentCourseService;
    }

    @GetMapping
    public List<StudentCourse> showAllStudentCourses(){
       try {
           return studentCourseService.getAll();
       }
       catch (IllegalArgumentException ex) {
           throw new ResponseStatusException(HttpStatus.NOT_FOUND, ex.getMessage());
       }
    }

    @PostMapping("/new")
    public StudentCourse create(@Valid @RequestBody StudentCourse studentCourse){
       try {
           return studentCourseService.create(studentCourse.getStudentUsername(), studentCourse.getCourseID());
       }
       catch (IllegalArgumentException ex){
           throw new ResponseStatusException(HttpStatus.CONFLICT, ex.getMessage());
       }
    }
}
