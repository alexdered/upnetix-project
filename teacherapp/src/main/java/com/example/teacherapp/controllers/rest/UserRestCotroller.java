package com.example.teacherapp.controllers.rest;

import com.example.teacherapp.models.User;
import com.example.teacherapp.services.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;

import javax.validation.Valid;

@RestController
@RequestMapping("/api/users")
public class UserRestCotroller {
   private UserService userService;

    @Autowired
    public UserRestCotroller(UserService userService) {
        this.userService = userService;
    }

    @PostMapping("/new")
    public User create(@Valid @RequestBody User user){
        try {
            return userService.create(user);
        }
        catch (IllegalArgumentException ex) {
            throw new ResponseStatusException(HttpStatus.CONFLICT, ex.getMessage());
        }
    }


}
