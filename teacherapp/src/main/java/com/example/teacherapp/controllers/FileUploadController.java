package com.example.teacherapp.controllers;

import com.example.teacherapp.models.Course;
import com.example.teacherapp.models.Homework;
import com.example.teacherapp.models.Lecture;
import com.example.teacherapp.services.CourseService;
import com.example.teacherapp.services.HomeworkService;
import com.example.teacherapp.services.LectureService;
import com.example.teacherapp.services.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

@Controller
public class FileUploadController {
    private HomeworkService homeworkService;
    private LectureService lectureService;
    private CourseService courseService;
    private UserService userService;
    public static String uploadDirectory = System.getProperty("user.dir")+"/uploads";

    @Autowired
    public FileUploadController(HomeworkService homeworkService, LectureService lectureService, CourseService courseService, UserService userService) {
        this.homeworkService = homeworkService;
        this.lectureService = lectureService;
        this.courseService = courseService;
        this.userService = userService;
    }

    @PostMapping("/uploadhomework/{lectureId}")
   public String fileUpload(@PathVariable int lectureId, @RequestParam("files") MultipartFile files){

        try{String studentUsername = userService.getSessionUser().getUsername();

        Lecture lecture= lectureService.getById(lectureId);
        Course course= courseService.getById(lecture.getCourseId());


        Path fileNameAndPath = Paths.get(uploadDirectory, files.getOriginalFilename());
        String  fileName = files.getOriginalFilename();
        try {
            Files.write(fileNameAndPath, files.getBytes());
        } catch (IOException e) {
            e.printStackTrace();
        }

        homeworkService.create(studentUsername,lecture.getCourseId(),lectureId, course.getTeacherUsername(), fileName, 0);

        return "redirect:/video/" + lectureId;}

        catch (Exception ex){
            return "AccessDenied";
        }
    }
}
