package com.example.teacherapp.controllers;

import com.example.teacherapp.models.*;
import com.example.teacherapp.services.CourseService;
import com.example.teacherapp.services.HomeworkService;
import com.example.teacherapp.services.LectureService;
import com.example.teacherapp.services.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;

import java.util.List;
import java.util.stream.Collectors;

@Controller
public class HomeworkController {
    private LectureService lectureService;
    private CourseService courseService;
    private UserService userService;
    private HomeworkService homeworkService;

    @Autowired
    public HomeworkController(LectureService lectureService, CourseService courseService, UserService userService, HomeworkService homeworkService) {
        this.lectureService = lectureService;
        this.courseService = courseService;
        this.userService = userService;
        this.homeworkService = homeworkService;
    }

    @GetMapping("/homework")
    public String showHomeworkView(Model model){

        try{
        String username = userService.getSessionUser().getUsername();

        List<Homework> homework= homeworkService.getAllForTeacher(username).stream().filter(p->p.getGrade()==0).sorted().collect(Collectors.toList());
        List<Course> teacherCourses = courseService.getAllActiveByTeacher(username);
        List<Lecture> lectures= lectureService.getAllL();

        model.addAttribute("homeworks" ,homework);
        model.addAttribute("courses" , teacherCourses);
        model.addAttribute("lectures" ,lectures);

        return "homework";}
        catch (Exception ex){
            return "AccessDenied";
        }
    }

    @PostMapping("/homework")
    public String submitGrade(@ModelAttribute Homework homework, Model model){

        try{
        System.out.println(homework.getId());
        System.out.println(homework.getGrade());

        return "redirect:/homework";}

        catch (Exception ex){
            return "AccessDenied";
        }
    }

}
