package com.example.teacherapp.controllers;

import com.example.teacherapp.models.*;
import com.example.teacherapp.services.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

    @Controller
    public class VideoLectureController {

    private StudentCourseService studentCourseService;
    private LectureService lectureService;
    private PassedLectureService passedLectureService;
    private HomeworkService homeworkService;
    private UserService userService;

    @Autowired
    public VideoLectureController(StudentCourseService studentCourseService, LectureService lectureService, PassedLectureService passedLectureService, HomeworkService homeworkService, UserService userService) {
        this.studentCourseService = studentCourseService;
        this.lectureService = lectureService;
        this.passedLectureService = passedLectureService;
        this.homeworkService = homeworkService;
        this.userService = userService;
    }

    @GetMapping("/video/{lectureId}")
    public String showLectureVideo(@PathVariable int lectureId, Model model) {

        try {
            String username = userService.getSessionUser().getUsername();

            Lecture lecture = lectureService.getById(lectureId);
            String youTubeVideoId = lecture.getVideoName();
            StudentCourse checkStudentCourse = studentCourseService.getById(username, lecture.getCourseId());
            PassedLecture passedLecture = passedLectureService.checkIfExist(username, lectureId);
            boolean existingHomework = homeworkService.existStudentByLecture(username, lectureId);

            Homework homeworkGrade = homeworkService.getAllForStudent(username).stream()
                    .filter(homework -> homework.getLectureId()== lectureId).findAny().orElse(null);

            model.addAttribute("lecture", lecture);
            model.addAttribute("youtubeId", youTubeVideoId);
            model.addAttribute("studentCourse", checkStudentCourse);
            model.addAttribute("passedLecture", passedLecture);
            model.addAttribute("checkIfExistHomework", existingHomework);
            model.addAttribute("homeworkWithGrade", homeworkGrade);

            return "videoLecture";
        } catch (Exception ex) {
            return "AccessDenied";
        }
    }


}
