package com.example.teacherapp.controllers;

import com.example.teacherapp.models.User;
import com.example.teacherapp.models.UserDTO;
import com.example.teacherapp.services.UserRequesService;
import com.example.teacherapp.services.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.AuthorityUtils;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.provisioning.UserDetailsManager;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;

import javax.validation.Valid;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;

@Controller
public class RegisterController {

    private UserService userService;
    private UserDetailsManager userDetailsManager;
    private UserRequesService userRequesService;
    public static String uploadDirectory = "./uploads/";


    @Autowired
    public RegisterController(UserService userService, UserDetailsManager userDetailsManager, UserRequesService userRequesService) {
        this.userService = userService;
        this.userDetailsManager = userDetailsManager;
        this.userRequesService = userRequesService;
    }

    @GetMapping("/register")
    public String showRegisterPage(Model model) {
        try {
            model.addAttribute("user", new UserDTO());
            return "register";
        } catch (Exception ex) {
            return "AccessDenied";
        }
    }

    @GetMapping("/register/teacher")
    public String showRegisterPageTeacher(Model model) {
        try {
            model.addAttribute("user", new UserDTO());
            return "registerTeacher";
        } catch (Exception ex) {
            return "AccessDenied";
        }
    }


    @PostMapping("/register")
    public String registerUser(@Valid @ModelAttribute("user") UserDTO user, BindingResult bindingResult, Model model, @RequestParam("files") MultipartFile file) {


        try {
            if (bindingResult.hasErrors()) {
                model.addAttribute("error", "Something is missing!");
                return "register";
            }

            if (userDetailsManager.userExists(user.getUsername())) {
                model.addAttribute("error", "User already exists!");
                return "register";
            }

            if (!user.getPassword().equals(user.getRePassword())) {
                model.addAttribute("error", "Password does not match!");
                return "register";
            }

            List<GrantedAuthority> authorities = AuthorityUtils.createAuthorityList("ROLE_USER");
            String encoded = new BCryptPasswordEncoder().encode(user.getPassword());
            org.springframework.security.core.userdetails.User newUser =
                    new org.springframework.security.core.userdetails.User(
                            user.getUsername(), "{bcrypt}" + encoded, authorities);
            userDetailsManager.createUser(newUser);

            Path fileNameAndPath = Paths.get(uploadDirectory, file.getOriginalFilename());
            String pictureName = file.getOriginalFilename();
            try {
                Files.write(fileNameAndPath, file.getBytes());
            } catch (IOException ex) {
                ex.printStackTrace();
            }
//todo take out in service
            User userInformation = new User();
            userInformation.setUsername(user.getUsername());
            userInformation.setFirstName(user.getFirstName());
            userInformation.setLastName(user.getLastName());
            userInformation.setEmail(user.getEmail());
            userInformation.setPicture(pictureName);

            userService.create(userInformation);
            return "redirect:/login";
        } catch (Exception ex) {
            return "AccessDenied";
        }

    }


    @PostMapping("/register/teacher")
    public String registerTeacher(@Valid @ModelAttribute("user") UserDTO user, BindingResult bindingResult, Model model, @RequestParam("files") MultipartFile file) {

        try {
            if (bindingResult.hasErrors()) {
                model.addAttribute("error", "Something is missing!");
                return "register";
            }

            if (userDetailsManager.userExists(user.getUsername())) {
                model.addAttribute("error", "User already exists!");
                return "register";
            }

            if (!user.getPassword().equals(user.getRePassword())) {
                model.addAttribute("error", "Password does not match!");
                return "register";
            }

            List<GrantedAuthority> authorities = AuthorityUtils.createAuthorityList("ROLE_USER");
            String encoded = new BCryptPasswordEncoder().encode(user.getPassword());
            org.springframework.security.core.userdetails.User newUser =
                    new org.springframework.security.core.userdetails.User(
                            user.getUsername(), "{bcrypt}" + encoded, authorities);
            userDetailsManager.createUser(newUser);

            Path fileNameAndPath = Paths.get(uploadDirectory, file.getOriginalFilename());
            String pictureName = file.getOriginalFilename();
            try {
                Files.write(fileNameAndPath, file.getBytes());
            } catch (IOException ex) {
                ex.printStackTrace();
            }

            userRequesService.create(user.getUsername(), 0);

            User userInformation = new User();
            userInformation.setUsername(user.getUsername());
            userInformation.setFirstName(user.getFirstName());
            userInformation.setLastName(user.getLastName());
            userInformation.setEmail(user.getEmail());
            userInformation.setPicture(pictureName);

            userService.create(userInformation);


            return "redirect:/login";
        } catch (Exception ex) {
            return "AccessDenied";
        }
    }


}
