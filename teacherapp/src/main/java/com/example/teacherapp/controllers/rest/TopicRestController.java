package com.example.teacherapp.controllers.rest;

import com.example.teacherapp.models.Topic;
import com.example.teacherapp.services.CourseService;
import com.example.teacherapp.services.TopicService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("/api/topics")
public class TopicRestController {

    private TopicService topicService;
    private CourseService courseService;

    @Autowired
    public TopicRestController(TopicService topicService, CourseService courseService) {
        this.topicService = topicService;
        this.courseService = courseService;
    }


    @GetMapping
    public List<Topic> getTopics(){
        try {
            return courseService.createCourseView();
        }
        catch (IllegalArgumentException ex) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, ex.getMessage());
        }
    }

    @PostMapping("/new")
    public Topic create(@Valid @RequestBody Topic topic){
    try {
        return topicService.saveTopic(topic);
    }
    catch (IllegalArgumentException ex) {
        throw new ResponseStatusException(HttpStatus.CONFLICT, ex.getMessage());
    }

    }

}
