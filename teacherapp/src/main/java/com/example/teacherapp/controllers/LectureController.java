package com.example.teacherapp.controllers;

import com.example.teacherapp.models.*;
import com.example.teacherapp.services.*;
import com.example.teacherapp.services.functional.ProgressTracker;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.validation.Valid;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.IllegalFormatPrecisionException;
import java.util.List;
import java.util.stream.Collectors;

@Controller
public class LectureController {
    private LectureService lectureService;
    private CourseService courseService;
    private StudentCourseService studentCourseService;
    private ProgressTracker progressTracker;
    private RatingService ratingService;
    private UserService userService;
    private CommentService commentService;

    public static String uploadDirectory = System.getProperty("user.dir") + "/uploads";

    @Autowired
    public LectureController(LectureService lectureService, CourseService courseService, StudentCourseService studentCourseService, ProgressTracker progressTracker, RatingService ratingService, UserService userService, CommentService commentService) {
        this.lectureService = lectureService;
        this.courseService = courseService;
        this.studentCourseService = studentCourseService;
        this.progressTracker = progressTracker;
        this.ratingService = ratingService;
        this.userService = userService;
        this.commentService = commentService;
    }

    @GetMapping("/lectures/{courseId}")
    public String showCourseInfo(@PathVariable int courseId, Model model) {


        try {

            StudentCourse checkStudentCourse = studentCourseService.getById(userService.getSessionUser().getUsername(), courseId);
            List<Lecture> courseLectures = lectureService.getAllForCourse(courseId);
            Course course = courseService.getById(courseId);
            int progress = progressTracker.totalProgress(userService.getSessionUser().getUsername(), courseId);
            List<Rating> checkIfRatingExist = ratingService.getByCourseAndStudent(userService.getSessionUser().getUsername(), courseId);
            RatingDTO ratingDto = new RatingDTO();
            Double courseRating = ratingService.getAverageCourseRating(courseId);

            List<Comment> allComments = commentService.getAllComments();
            List<Comment> particularCourse = allComments.stream().filter(p -> p.getCourseId() == courseId).collect(Collectors.toList());
            List<Comment> latestThree = new ArrayList<>();
            if (particularCourse.size() > 0) {
                latestThree.add(particularCourse.get(particularCourse.size() - 1));
            }
            if (particularCourse.size() > 1) {
                latestThree.add(particularCourse.get(particularCourse.size() - 2));
            }
            if (particularCourse.size() > 2) {
                latestThree.add(particularCourse.get(particularCourse.size() - 3));
            }

            model.addAttribute("allComments", latestThree);

            model.addAttribute("studentCourse", checkStudentCourse);
            model.addAttribute("lectures", courseLectures);
            model.addAttribute("course", course);
            model.addAttribute("progress", progress);
            model.addAttribute("checkRating", checkIfRatingExist);
            model.addAttribute("ratingDTO", ratingDto);
            model.addAttribute("courseRating", courseRating);


            return "courseInfo";
        } catch (Exception ex) {
            List<Lecture> courseLectures = lectureService.getAllForCourse(courseId);
            Course course = courseService.getById(courseId);
            model.addAttribute("lectures", courseLectures);
            model.addAttribute("course", course);
            return "courseInfoII";
        }

    }

    @GetMapping("/lectures/enroll/{courseId}")
    public String enrollInCourse(@PathVariable int courseId) {

        studentCourseService.create(userService.getSessionUser().getUsername(), courseId);

        return "redirect:/lectures/" + courseId;
    }


    @GetMapping("/lecture/new")
    public String createLectureView(Model model) {

        List<Course> teachersCourses = courseService.getAllActiveByTeacher(userService.getSessionUser().getUsername());
        model.addAttribute("lecture", new Lecture());
        model.addAttribute("courses", teachersCourses);
        return "createLecture";
    }

    //TODO
    @PostMapping("/lecture/new")
    public String createNewLecture(@Valid @ModelAttribute Lecture lecture, @RequestParam("files") MultipartFile file) throws IOException {

        Path fileNameAndPath = Paths.get(uploadDirectory, file.getOriginalFilename());
        String fileName = file.getOriginalFilename();

        Files.write(fileNameAndPath, file.getBytes());

        lectureService.create(lecture.getTitle(), lecture.getDescription(), lecture.getVideoName(), fileName, lecture.getCourseId());
        return "redirect:/lecture/new";
    }
}
