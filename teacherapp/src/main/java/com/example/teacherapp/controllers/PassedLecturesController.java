package com.example.teacherapp.controllers;

import com.example.teacherapp.models.Lecture;
import com.example.teacherapp.services.LectureService;
import com.example.teacherapp.services.PassedLectureService;
import com.example.teacherapp.services.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

    @Controller
    public class PassedLecturesController {

    private PassedLectureService passedLectureService;
    private LectureService lectureService;
    private UserService userService;

    @Autowired
    public PassedLecturesController(PassedLectureService passedLectureService, LectureService lectureService, UserService userService) {
        this.passedLectureService = passedLectureService;
        this.lectureService = lectureService;
        this.userService = userService;
    }

    @GetMapping("/passedlecture/new/{lectureId}")
    public String createPassedLecture(@PathVariable int lectureId) {

        try {
            String username = userService.getSessionUser().getUsername();
            Lecture lecture = lectureService.getById(lectureId);
            int courseId = lecture.getCourseId();
            passedLectureService.create(username, courseId, lectureId);
            return "redirect:/video/" + lecture.getId();
        } catch (Exception ex) {
            return "AccessDenied";
        }
    }
}
