package com.example.teacherapp.controllers;

import com.example.teacherapp.models.RatingDTO;
import com.example.teacherapp.services.RatingService;
import com.example.teacherapp.services.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;

    @Controller
    public class RatingController {
    private RatingService ratingService;
    private UserService userService;

    @Autowired
    public RatingController(RatingService ratingService, UserService userService) {
        this.ratingService = ratingService;
        this.userService = userService;
    }

    @PostMapping("/rate/{courseId}")
    public String rateCourse(@ModelAttribute RatingDTO ratingDto, @PathVariable int courseId) throws IllegalAccessException {

        try {
            String username = userService.getSessionUser().getUsername();
            ratingService.createRatingForCourse(username, courseId, ratingDto.getRating());
            return "redirect:/lectures/{courseId}";
        } catch (Exception ex) {
            return "AccessDenied";
        }
    }
}
