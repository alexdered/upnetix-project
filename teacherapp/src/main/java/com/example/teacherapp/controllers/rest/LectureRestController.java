package com.example.teacherapp.controllers.rest;

import com.example.teacherapp.models.Lecture;
import com.example.teacherapp.services.LectureService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import java.util.List;

@RestController
@RequestMapping("api/lectures")
public class LectureRestController {
    private LectureService lectureService;

    @Autowired
    public LectureRestController(LectureService lectureService) {
        this.lectureService = lectureService;
    }

    @GetMapping("/{courseId}")
    public List<Lecture> allLectures(@PathVariable int courseId){
        try {
            return lectureService.getAllForCourse(courseId);
        }
        catch (IllegalArgumentException ex) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, ex.getMessage());
        }
    }

    @PostMapping("/new")
    public Lecture addLecture(@RequestBody Lecture lecture){
        try{
            return lectureService.create(lecture.getTitle(), lecture.getDescription(),
                    lecture.getVideoName() , lecture.getAssignmentName(), lecture.getCourseId());
        } catch (IllegalArgumentException ex) {
            throw new ResponseStatusException(HttpStatus.CONFLICT, ex.getMessage());
        }
    }
}
