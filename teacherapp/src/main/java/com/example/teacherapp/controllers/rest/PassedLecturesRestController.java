package com.example.teacherapp.controllers.rest;

import com.example.teacherapp.models.PassedLecture;
import com.example.teacherapp.services.PassedLectureService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("/api/passedlectures")
public class PassedLecturesRestController {
    private PassedLectureService passedLectureService;

    @Autowired
    public PassedLecturesRestController(PassedLectureService passedLectureService) {
        this.passedLectureService = passedLectureService;
    }

    @GetMapping
    public List<PassedLecture> allPassedLecutes(){
        try {
            return passedLectureService.getAll();
        }catch (IllegalArgumentException ex) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, ex.getMessage());
        }
    }

    @PostMapping("/new")
    public PassedLecture create(@RequestBody PassedLecture passedLecture){
        try {
            return passedLectureService.create(passedLecture.getStudentName(), passedLecture.getCourseId(), passedLecture.getLectureId());
        }catch (IllegalArgumentException ex) {
            throw new ResponseStatusException(HttpStatus.CONFLICT, ex.getMessage());
        }
    }
}
