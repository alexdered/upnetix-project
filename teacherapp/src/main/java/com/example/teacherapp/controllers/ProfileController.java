package com.example.teacherapp.controllers;

import com.example.teacherapp.models.User;
import com.example.teacherapp.services.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;

import javax.validation.Valid;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;


@Controller
public class ProfileController {

    public static String uploadDirectory = "./uploads";
    private UserService userService;

    @Autowired
    public ProfileController(UserService userService) {
        this.userService = userService;
    }

    @GetMapping("/myProfile")
    public String showHomePage(Model model, User user) {

        try {
            String username = userService.getSessionUser().getUsername();
            List<User> allUsers = userService.getAll();
            user = allUsers.stream().filter(p -> p.getUsername().equals(username)).findAny().orElseThrow(IllegalArgumentException::new);

            model.addAttribute("path", "./uploads/" + user.getPicture());
            model.addAttribute("user", user);
            String updateNum = "";
            model.addAttribute("updateNum", updateNum);
            String updatedValue = "";
            model.addAttribute("updatedValue", updatedValue);

            return "myProfile";
        } catch (Exception ex) {
            return "AccessDenied";
        }
    }


    @PostMapping("/myProfile")
    public String updateUser(@Valid @ModelAttribute User user, @ModelAttribute String updateNum, @ModelAttribute String updatedValue, Model model, @RequestParam("files") MultipartFile[] files) {

        try {
            String newPicName = "";
            for (MultipartFile file : files) {
                newPicName = file.getOriginalFilename();
                Path fileNameAndPath = Paths.get(uploadDirectory, file.getOriginalFilename());
                try {
                    Files.write(fileNameAndPath, file.getBytes());
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }

            user.setPicture(newPicName);
            userService.updateUser(user);

            return "redirect:/myProfile";
        } catch (Exception ex) {
            return "AccessDenied";
        }

    }
}
