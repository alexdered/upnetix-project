package com.example.teacherapp.controllers;


import com.example.teacherapp.models.Course;
import com.example.teacherapp.models.Topic;
import com.example.teacherapp.services.CourseService;
import com.example.teacherapp.services.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;

import java.util.List;


@Controller
public class HomeController {

    private CourseService courseService;
    private UserService userService;

    @Autowired
    public HomeController(CourseService courseService, UserService userService) {
        this.courseService = courseService;
        this.userService = userService;
    }

    @GetMapping("/home")
    public String HomePage(Model model) {

        try {
            List<Course> courses = courseService.getTopThree();
            List<Topic> topics = courseService.createCourseView();
            int allCourses = courseService.getAll().size();
            int allStudents = userService.getAllStudents().size();
            int allTeachers = userService.getAllTeachers().size();

            model.addAttribute("courses", courses);
            model.addAttribute("topics", topics);
            model.addAttribute("coursesCounter", allCourses);
            model.addAttribute("studentsCounter", allStudents);
            model.addAttribute("teachersCounter", allTeachers);

            return "home";

        } catch (Exception ex) {
            return "AccessDenied";
        }
    }

    @GetMapping("/")
    public String showIndexPage(Model model) {

        try {
            List<Course> courses = courseService.getTopThree();
            List<Topic> topics = courseService.createCourseView();
            model.addAttribute("courses", courses);
            model.addAttribute("topics", topics);

            return "index";
        } catch (Exception ex) {
            return "AccessDenied";
        }
    }

}
