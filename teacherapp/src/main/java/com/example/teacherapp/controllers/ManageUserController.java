package com.example.teacherapp.controllers;

import com.example.teacherapp.models.User;
import com.example.teacherapp.models.UserRequest;
import com.example.teacherapp.services.UserRequesService;
import com.example.teacherapp.services.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.provisioning.UserDetailsManager;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;

import java.util.ArrayList;
import java.util.List;


@Controller
public class ManageUserController {
    private UserService userService;
    private UserRequesService userRequesService;


    @Autowired
    public ManageUserController(UserService userService, UserRequesService userRequesService, UserDetailsManager userDetailsManager) {
        this.userService = userService;
        this.userRequesService = userRequesService;
    }

    @GetMapping("/users")
    public String showMenageUsersPage(Model model) {
        try {
            List<User> users = userService.getAll();
            model.addAttribute("users", users);
            return "users";
        } catch (IllegalArgumentException ex) {
            return "AccessDenied";
        }
    }

    @GetMapping("/teacher/requests")
    public String showTeacherRequests(Model model) {
        try {
            List<UserRequest> requests = userRequesService.getAllTeacherRequests();
            List<User> users = userService.getAll();
            model.addAttribute("usersInfo", users);
            model.addAttribute("requests", requests);
            return "teacherRequests";
        } catch (Exception ex) {
            return "AccessDenied";
        }


    }


    @GetMapping("/admin/requests")
    public String chooseNewAdminPage(Model model) {
        try {

            List<User> adminPretenders = userService.getAdminPretenders();

            model.addAttribute("users", adminPretenders);
            return "usersAdmins";
        } catch (IllegalArgumentException ex) {
            return "AccessDenied";
        }
    }

    @GetMapping("/users/deletion")
    public String deleteUserPage(Model model) {
        try {
            List<User> users = userService.getAll();
            model.addAttribute("users", users);
            return "usersDeletion";
        } catch (IllegalArgumentException ex) {
            return "AccessDenied";
        }
    }

    @GetMapping("/teacher/request/new")
    public String sendTeacherRequest() {
        try {
            String username = userService.getSessionUser().getUsername();
            userRequesService.create(username, 0);
            return "redirect:/";
        } catch (Exception ex) {
            return "AccessDenied";
        }
    }


    @GetMapping("/teacher/newrequest")
    public String sendNewTeacherRequest() {
        try {
            String username = userService.getSessionUser().getUsername();
            userRequesService.create(username, 0);
            return "home";
        } catch (Exception ex) {
            return "AccessDenied";
        }
    }

}
