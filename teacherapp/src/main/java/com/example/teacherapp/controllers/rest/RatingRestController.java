package com.example.teacherapp.controllers.rest;

import com.example.teacherapp.models.Rating;
import com.example.teacherapp.services.RatingService;
import com.example.teacherapp.services.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("/api/")
public class RatingRestController {

    private RatingService ratingService;
    private UserService userService;

    @Autowired
    public RatingRestController( RatingService ratingService, UserService userService) {

        this.ratingService = ratingService;
        this.userService = userService;
    }

    @PostMapping("/rate")
    public Rating rateCourse(@Valid @RequestParam(value = "stars") int stars,
                             @RequestParam(value = "id") int id) {
        Rating rating;
        try {

            rating = ratingService.createRatingForCourse(userService.getSessionUser().getUsername(), id, stars);
            return rating;

        } catch (IllegalArgumentException ex) {
            throw new ResponseStatusException(HttpStatus.CONFLICT, ex.getMessage());
        }

    }

    @GetMapping("/rate/{courseId}")
    public Double getAverageRatingForCourse(@Valid @PathVariable int courseId) {
        try {
            return ratingService.getAverageCourseRating(courseId);
        } catch (IllegalArgumentException ex) {
            throw new ResponseStatusException(HttpStatus.CONFLICT, ex.getMessage());
        }
    }


    @GetMapping("/rated")
    public List<Integer> getAvgRatedCourses() {
        try {
            return ratingService.getAllRatedCoursesWithAvgRating();
        } catch (IllegalArgumentException ex) {
            throw new ResponseStatusException(HttpStatus.CONFLICT, ex.getMessage());
        }
    }

}

