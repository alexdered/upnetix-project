package com.example.teacherapp.controllers.rest;

import com.example.teacherapp.models.Course;
import com.example.teacherapp.services.CourseService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import javax.validation.Valid;
import java.util.List;
import java.util.Set;

@RestController
@RequestMapping("api/courses")
public class CourseRestController {
    private CourseService courseService;

    @Autowired
    public CourseRestController(CourseService courseService) {
        this.courseService = courseService;
    }

    @GetMapping
    public List<Course> getAll() {
        try {
            return courseService.getAll();
        } catch (IllegalArgumentException ex) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, ex.getMessage());
        }
    }

    @GetMapping("/active")
    public List<Course> getAllActive() {
        try {
            return courseService.getAllActive();
        } catch (IllegalArgumentException ex) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, ex.getMessage());
        }
    }

    @PostMapping("/new")
    public Course createCourse(@Valid @RequestBody Course course) {
        try {
            return courseService.create(course.getTitle(), course.getDescription(), course.getTeacherUsername(), course.getTopicId(), course.getPictureName());
        } catch (IllegalArgumentException ex) {
            throw new ResponseStatusException(HttpStatus.CONFLICT, ex.getMessage());
        }
    }

    @GetMapping("/{id}")
    public Course getByIdCourse(@PathVariable int id) {
        try {
            return courseService.getById(id);
        } catch (IllegalArgumentException ex) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, ex.getMessage());
        }
    }

    @PutMapping("/delete/{id}")
    public Course deleteCourse(@PathVariable int id) {
        try {
            return courseService.delete(id);
        } catch (IllegalArgumentException ex) {
            throw new ResponseStatusException(HttpStatus.CONFLICT, ex.getMessage());
        }
    }

    @GetMapping("/ordered/rating")
    public List<Course> getAllOrderedByRating() {
        try {
            return courseService.getAllOrderedByRating();
        } catch (IllegalArgumentException ex) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, ex.getMessage());
        }
    }

    @GetMapping("/filtered")
    public Set<Course> getAllFilteredByTitle() {
        try {
            return courseService.getAllFilteredByTitle("programming");
        } catch (IllegalArgumentException ex) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, ex.getMessage());
        }
    }


}
