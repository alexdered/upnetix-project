package com.example.teacherapp.controllers;

import com.example.teacherapp.models.Comment;
import com.example.teacherapp.services.CommentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;

import java.util.List;

@Controller
public class CommentController {

 private CommentService commentService;

    @Autowired
    public CommentController(CommentService commentService) {
        this.commentService = commentService;
    }

    @GetMapping("/comments")
    public String displayComments(Model model){

        List<Comment> allComments = commentService.getAllComments();
        model.addAttribute("allComments",allComments);

        return "courseInfo";
}

}
