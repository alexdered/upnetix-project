package com.example.teacherapp.controllers;

import com.example.teacherapp.services.SendEmailService;
import com.example.teacherapp.services.SendEmailServiceImpl;
import com.example.teacherapp.services.UserRequesService;
import com.example.teacherapp.services.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.AuthorityUtils;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.provisioning.UserDetailsManager;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;



import java.util.List;

@Controller
public class UserDetailsController {

    private UserDetailsManager userDetailsManager;
    private UserRequesService userRequesService;
    private UserService userService;
    private SendEmailService sendEmailService;

    @Autowired
    public UserDetailsController(UserDetailsManager userDetailsManager, UserRequesService userRequesService, UserService userService, SendEmailService sendEmailService) {
        this.userDetailsManager = userDetailsManager;
        this.userRequesService = userRequesService;
        this.userService = userService;
        this.sendEmailService = sendEmailService;
    }

    @GetMapping("/teacher/approve/{username}")
    public String approveTeacher(@PathVariable String username) {

        try {
            List<GrantedAuthority> authorities = AuthorityUtils.createAuthorityList("ROLE_USER", "ROLE_TEACHER");
            UserDetails oldDetails = userDetailsManager.loadUserByUsername(username);
            UserDetails newDetails = new User(username, oldDetails.getPassword(), authorities);
            userDetailsManager.updateUser(newDetails);
            userRequesService.approveRequest(username, 0);

            sendEmailService.sendConfirmationEmail(userService.getByUsername(username).getEmail());

            return "redirect:/teacher/requests";
        } catch (Exception ex) {
            return "AccessDenied";
        }
    }


    @GetMapping("/teacher/reject/{username}")
    public String rejectAdmin(@PathVariable String username) {

        try {
            userRequesService.approveRequest(username, 0);
            return "redirect:/teacher/requests";
        } catch (Exception ex) {
            return "AccessDenied";
        }
    }


    @GetMapping("/admin/approve/{username}")
    public String approveAdmin(@PathVariable String username) {

        try {
            List<GrantedAuthority> authorities = AuthorityUtils.createAuthorityList("ROLE_USER", "ROLE_TEACHER", "ROLE_ADMIN");
            UserDetails oldDetails = userDetailsManager.loadUserByUsername(username);
            UserDetails newDetails = new User(username, oldDetails.getPassword(), authorities);
            userDetailsManager.updateUser(newDetails);

            return "redirect:/admin/requests";
        } catch (Exception ex) {
            return "AccessDenied";
        }
    }


    @GetMapping("/user/delete/{username}")
    public String deleteUser(@PathVariable String username) {

        try {

            List<GrantedAuthority> authorities = AuthorityUtils.createAuthorityList();
            UserDetails oldDetails = userDetailsManager.loadUserByUsername(username);
            UserDetails newDetails = new User(username, oldDetails.getPassword(), authorities);
            userDetailsManager.updateUser(newDetails);

            com.example.teacherapp.models.User user = userService.getByUsername(username);
            userService.delete(user);

            return "redirect:/users/deletion";
        } catch (Exception ex) {
            return "AccessDenied";
        }
    }


}
