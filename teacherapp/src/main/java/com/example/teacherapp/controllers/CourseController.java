package com.example.teacherapp.controllers;

import com.example.teacherapp.models.Course;
import com.example.teacherapp.models.CourseDTO;
import com.example.teacherapp.models.Topic;
import com.example.teacherapp.services.CourseService;
import com.example.teacherapp.services.StudentCourseService;
import com.example.teacherapp.services.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import javax.jws.WebParam;
import javax.validation.Valid;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;
import java.util.Set;

@Controller
public class CourseController extends ResponseEntityExceptionHandler {

    private CourseService courseService;
    private StudentCourseService studentCourseService;
    private UserService userService;
    public static String uploadDirectory = System.getProperty("user.dir") + "/uploads";

    @Autowired
    public CourseController(CourseService courseService, StudentCourseService studentCourseService, UserService userService) {
        this.courseService = courseService;
        this.studentCourseService = studentCourseService;
        this.userService = userService;
    }

    @GetMapping("/menageCourses")
    public String showMenageCoursesPage(Model model) {

        String username = userService.getSessionUser().getUsername();

        List<Course> myCourses = courseService.getAllActiveByTeacher(username);
        List<Topic> topics = courseService.createCourseView();
        model.addAttribute("topics", topics);
        model.addAttribute("myCourses", myCourses);
        return "MenageCourses";
    }

    @GetMapping("/createCourse")
    public String showCreateCourseView(Model model) {

        List<Topic> topics = courseService.createCourseView();
        model.addAttribute("topics", topics);
        model.addAttribute("course", new Course());
        return "createCourse";
    }

    @PostMapping("/createCourse")
    public String createCourse(@Valid @ModelAttribute Course course, @ModelAttribute Topic topic, BindingResult bindingResult, Model model,
                               @RequestParam("files") MultipartFile file) {

        if (bindingResult.hasErrors()) {
            model.addAttribute("error", "Something is missing!");
            return "createCourse";
        }

        Path fileNameAndPath = Paths.get(uploadDirectory, file.getOriginalFilename());
        try {
            Files.write(fileNameAndPath, file.getBytes());
        } catch (IOException ex) {
            ex.printStackTrace();
        }

        Object principal = SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        String username = ((UserDetails) principal).getUsername();


        try {
            courseService.create(course.getTitle(), course.getDescription(), username, course.getTopicId(), "/uploads/" + file.getOriginalFilename());
        } catch (IllegalArgumentException ex) {
            model.addAttribute("error", ex.getMessage());
            return "createCourse";
        }


        return "redirect:/lecture/new";
    }

    @GetMapping("/courses")
    public String showCourses(Model model) {

        List<Course> allCourses = courseService.getAllActivePaged(1, 0);
        List<Topic> topics = courseService.createCourseView();
        int switchStyle = 1;

        model.addAttribute("allCourses", allCourses);
        model.addAttribute("topics", topics);

        model.addAttribute("switch", switchStyle);
        model.addAttribute("courseDto", new CourseDTO());


        return "courses";
    }

    @GetMapping("/courses/next/{page}")
    public String nextPage(@PathVariable int page, Model model) {

        List<Course> allCourses = courseService.getAllActivePaged(page, 1);
        List<Topic> topics = courseService.createCourseView();
        int switchStyle = 1;

        model.addAttribute("page", page);
        model.addAttribute("allCourses", allCourses);
        model.addAttribute("topics", topics);

        model.addAttribute("switch", switchStyle);
        model.addAttribute("courseDto", new CourseDTO());


        return "coursesNext";
    }

    @GetMapping("/courses/ordered")
    public String showOrderedByName(Model model) {

        List<Course> orderedCourses = courseService.getAllOrderedByTitle(1, 0);
        List<Topic> topics = courseService.createCourseView();
        int switchStyle = 2;

        model.addAttribute("allCourses", orderedCourses);
        model.addAttribute("topics", topics);
        model.addAttribute("switch", switchStyle);

        model.addAttribute("courseDto", new CourseDTO());

        return "courses";

    }

    @GetMapping("/courses/top")
    public String showTopRated(Model model) {

        List<Course> orderedCourses = courseService.getAllOrderedByRating();
        List<Topic> topics = courseService.createCourseView();

        model.addAttribute("allCourses", orderedCourses);
        model.addAttribute("topics", topics);

        model.addAttribute("courseDto", new CourseDTO());

        return "courses";

    }

    @GetMapping("/courses/ordered/next/{page}")
    public String nextOrderedPage(@PathVariable int page, Model model) {

        List<Course> allCourses = courseService.getAllOrderedByTitle(page, 1);
        List<Topic> topics = courseService.createCourseView();
        int switchStyle = 2;

        model.addAttribute("page", page);
        model.addAttribute("allCourses", allCourses);
        model.addAttribute("topics", topics);

        model.addAttribute("switch", switchStyle);
        model.addAttribute("courseDto", new CourseDTO());


        return "coursesNext";
    }

    @PostMapping("/filtered")
    public String showFilteredCourses(@ModelAttribute CourseDTO course, Model model) {

        Set<Course> courses = courseService.getAllFilteredByTitle(course.getText());
        List<Topic> topics = courseService.createCourseView();

        model.addAttribute("allCourses", courses);
        model.addAttribute("topics", topics);

        model.addAttribute("courseDto", new CourseDTO());

        return "coursesFiltered";
    }

    @GetMapping("/courses/ongoing")
    public String showOngoingCourses(Model model) {


        String username = userService.getSessionUser().getUsername();

        List<Course> studentOngoingCourses = studentCourseService.getAllActiveOngoingForStudent(username);
        List<Topic> topics = courseService.createCourseView();


        model.addAttribute("topics", topics);
        model.addAttribute("ongoingCourses", studentOngoingCourses);

        return "ongoingCourses";
    }

    @GetMapping("/courses/finished")
    public String showFinishedCourses(Model model) {


        String username = userService.getSessionUser().getUsername();

        List<Course> studentFinishedCourses = studentCourseService.getAllActiveFinishedForStudent(username);
        List<Topic> topics = courseService.createCourseView();


        model.addAttribute("topics", topics);
        model.addAttribute("finishedCourses", studentFinishedCourses);

        return "finishedCourses";
    }

    @GetMapping("/deletecourse")
    public String deleteMyCourseView(Model model) {
        try {
            String username = userService.getSessionUser().getUsername();

            List<Course> teachersCourses = courseService.getAllActiveByTeacher(username);
            model.addAttribute("courses", teachersCourses);

            return "deleteCourseView";
        } catch (IllegalArgumentException ex) {
            return "AccessDenied";
        }

    }

    @GetMapping("/deletecourse/all")
    public String deleteAllCourseView(Model model) {
        List<Course> allActiveCourses = courseService.getAllActive();
        model.addAttribute("allActiveCourses", allActiveCourses);

        return "deleteAllCourseView";

    }

    @GetMapping("/deletecourse/{courseId}")
    public String deleteMyCourse(@PathVariable int courseId) {

        try {
            courseService.delete(courseId);
            return "redirect:/deletecourse";
        } catch (Exception ex) {
            return "AccessDenied";
        }
    }


}
