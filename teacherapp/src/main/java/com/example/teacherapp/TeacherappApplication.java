package com.example.teacherapp;

import com.example.teacherapp.controllers.RegisterController;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import java.io.File;

@SpringBootApplication
public class TeacherappApplication {
    public static void main(String[] args) {
        new File(RegisterController.uploadDirectory).mkdir();
        SpringApplication.run(TeacherappApplication.class, args);
    }

}
