package com.example.teacherapp.config;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.core.env.Environment;
import org.springframework.jdbc.datasource.DriverManagerDataSource;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.provisioning.JdbcUserDetailsManager;
import org.springframework.security.provisioning.UserDetailsManager;

import javax.sql.DataSource;

@Configuration
@EnableWebSecurity
@PropertySource("classpath:application.properties")
public class SecurityConfig extends WebSecurityConfigurerAdapter {

    private String dbUrl, dbUsername, dbPassword;

    public SecurityConfig(Environment env) {

        dbUrl = env.getProperty("database.url");
        dbUsername = env.getProperty("database.username");
        dbPassword = env.getProperty("database.password");
    }

    @Override
    protected void configure(AuthenticationManagerBuilder auth) throws Exception {
        auth.jdbcAuthentication().dataSource(securityDataSource());
    }


    @Bean
    public DataSource securityDataSource() {
        DriverManagerDataSource dataSource = new DriverManagerDataSource();
        dataSource.setDriverClassName("com.mysql.cj.jdbc.Driver");
        dataSource.setUrl(dbUrl);
        dataSource.setUsername(dbUsername);
        dataSource.setPassword(dbPassword);

        return dataSource;
    }

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http.csrf().disable()
                .authorizeRequests()

                .antMatchers("/api/**").permitAll()

                .antMatchers( "/lectures/**").permitAll()

                .antMatchers("/users").hasRole("ADMIN")
                .antMatchers("/deletecourse/all").hasRole("ADMIN")

                .antMatchers("/homework").hasRole("TEACHER")
                .antMatchers("/menageCourses").hasRole("TEACHER")
                .antMatchers("/createCourse").hasRole("TEACHER")
                .antMatchers("/deletecourse").hasRole("TEACHER")
                .antMatchers("/teacher/requests").hasRole("TEACHER")
                .antMatchers("/admin/requests").hasRole("TEACHER")
                .antMatchers("/users/deletion").hasRole("TEACHER")
                .antMatchers("/lectures/new").hasRole("TEACHER")

                .antMatchers( "/uploads/**").permitAll()
                .antMatchers("/css/**", "/js/**", "/images/**", "/fonts/**" , "/realImg/**").permitAll()
                .antMatchers("/static/**").permitAll()
                .antMatchers("/login").permitAll()
                .antMatchers("/register").permitAll()
                .antMatchers("/register/teacher").permitAll()
                .antMatchers("/courses").permitAll()
                .antMatchers("/courses/next/**").permitAll()
                .antMatchers("/courses/ordered/**").permitAll()
                .antMatchers("/courses/top").permitAll()
                .antMatchers("/filtered").permitAll()
                .anyRequest()
                .authenticated()
                .antMatchers("/").hasRole("USER")


                .and()
                .formLogin()
                .loginPage("/home")
                .loginProcessingUrl("/authenticate")
                .permitAll()
                .and()
                .logout().
                permitAll()
                .and().exceptionHandling().accessDeniedPage("/access-denied");
    }


    @Autowired
    private DataSource securityDataSource;

    @Bean
    public UserDetailsManager userDetailsManager() {
        JdbcUserDetailsManager jdbcUserDetailsManager = new JdbcUserDetailsManager();
        jdbcUserDetailsManager.setDataSource(securityDataSource);
        return jdbcUserDetailsManager;
    }


}