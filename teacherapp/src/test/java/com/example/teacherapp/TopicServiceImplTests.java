package com.example.teacherapp;

import com.example.teacherapp.models.Topic;
import com.example.teacherapp.models.User;
import com.example.teacherapp.repositories.TopicRepo;
import com.example.teacherapp.services.TopicServiceImpl;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

@RunWith(MockitoJUnitRunner.class)
public class TopicServiceImplTests {

    @Mock
    private TopicRepo repository;

    @InjectMocks
    private TopicServiceImpl service;

    @Test
    public void shouldSaveTopic(){
        //Arrange
        Topic topic = new Topic("MyTopic");
        //Act
        service.saveTopic(topic);
        //Assert
        verify(repository, times(1)).saveTopic(topic);
    }


}
