package com.example.teacherapp;

import com.example.teacherapp.models.Course;
import com.example.teacherapp.repositories.CourseRepo;
import com.example.teacherapp.services.CourseServiceImpl;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.ArrayList;
import java.util.List;

import static org.mockito.Mockito.*;


@RunWith(MockitoJUnitRunner.class)
public class CourseServiceImplTests {

    @Mock
    private CourseRepo courseRepo;

    @InjectMocks
    private CourseServiceImpl service;


    @Test
    public void create_Should_CreateInRepository_When_CourseIsValid() {

        //Arrange
        String title = "NewCourse";
        int topicId = 1;
        String description = "Some description";
        String teacherUsername = "Bob Martin";
        String pictureName = "course.png";

        Course course = new Course(title, topicId, description, teacherUsername, pictureName);

        //Act
        service.create(course.getTitle(), course.getDescription(), course.getTeacherUsername(), course.getTopicId(), course.getPictureName());

        //Assert

        verify(courseRepo, times(1)).create(course);

    }

    @Test(expected = IllegalArgumentException.class)
    public void create_Should_ThrowsException_When_CourseIsNotValid() {

        //Arrange
        String title = "NewCourse";
        int topicId = 1;
        String description = "Some description";
        String teacherUsername = "Bob Martin";
        String pictureName = "course.png";

        Course course = new Course(title, topicId, description, teacherUsername, pictureName);

        List<Course> list = new ArrayList<>();
        list.add(course);

        when(courseRepo.checkIfExists(title)).thenReturn(list);

        //Act
        service.create(course.getTitle(), course.getDescription(), course.getTeacherUsername(), course.getTopicId(), course.getPictureName());

    }


    @Test
    public void getAllActive_Should_ReturnAllActive() {

        //Arrange
        String title = "NewCourse";
        String titleII = "NewCourse2";
        String titleIII = "NewCourse3";
        int topicId = 1;
        String description = "Some description";
        String teacherUsername = "Bob Martin";
        String pictureName = "course.png";

        Course course1 = new Course(title, topicId, description, teacherUsername, pictureName);
        Course course2 = new Course(titleII, topicId, description, teacherUsername, pictureName);
        Course course3 = new Course(titleIII, topicId, description, teacherUsername, pictureName);

        course2.setDeleted(true);

        List<Course> courses = new ArrayList<>();
        courses.add(course1);
        courses.add(course2);
        courses.add(course3);

        List<Course> expectedCourses = new ArrayList<>();
        expectedCourses.add(course1);
        expectedCourses.add(course3);

        when(service.getAllActive()).thenReturn(expectedCourses);
        //Act

        List<Course> actual = service.getAllActive();

        //Assert
        Assert.assertEquals(expectedCourses,actual);

    }

    @Test
    public void getAllActiveByTeacher_Should_ReturnAllActiveByTeacher_When_CourseExists() {

        //Arrange
        String teacherUsername = "Bob Martin";

        String title = "NewCourse";
        String titleII = "NewCourse2";
        int topicId = 1;
        String description = "Some description";
        String pictureName = "course.png";

        Course course1 = new Course(title, topicId, description, teacherUsername, pictureName);
        Course course2 = new Course(titleII, topicId, description, teacherUsername, pictureName);


        List<Course> courses = new ArrayList<>();
        courses.add(course1);
        courses.add(course2);

        List<Course> expectedCourses = new ArrayList<>();
        expectedCourses.add(course1);
        expectedCourses.add(course2);

        when(courseRepo.getByTeacher(teacherUsername)).thenReturn(courses);
        //Act

        List<Course> actual = service.getAllActiveByTeacher(teacherUsername);

        //Assert
        Assert.assertEquals(expectedCourses,actual);

    }


    @Test
    public void getById_Should_ReturnCourse_When_CourseExists() {

        //Arrange

        int randomCourseId= 1;
        String title = "NewCourse";
        int topicId = 1;
        String description = "Some description";
        String teacherUsername = "Bob Martin";
        String pictureName = "course.png";

        Course course = new Course(title, topicId, description, teacherUsername, pictureName);

        when(courseRepo.getById(randomCourseId)).thenReturn(course);

        //Act

        Course actual = service.getById(randomCourseId);

        //Assert
        Assert.assertEquals(course,actual);
    }

//    @Test
//    public void delete_Should_DeleteCourseFromRepo_When_CourseExists(){
//
//        //Arrange
//
//        int randomId= 1;
//        String title = "NewCourse";
//        int topicId = 1;
//        String description = "Some description";
//        String teacherUsername = "Bob Martin";
//        String pictureName = "course.png";
//
//        Course course = new Course(title, topicId, description, teacherUsername, pictureName);
//        course.setId(randomId);
//
//
//
//        List<Course> activeCourses = new ArrayList<>();
//        activeCourses.add(course);
//
//        when(service.getAllActiveCourses()).thenReturn(activeCourses);
//        //Act
//        Course courseToDelete = service.delete(randomId);
//
//        //Assert
//        verify(courseRepo, times(1)).update(courseToDelete);
//
//    }


}
