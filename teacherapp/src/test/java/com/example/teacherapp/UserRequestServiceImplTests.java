package com.example.teacherapp;

import com.example.teacherapp.repositories.UserRequestRepo;
import com.example.teacherapp.services.UserRequestServiceImpl;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

@RunWith(MockitoJUnitRunner.class)
public class UserRequestServiceImplTests {

    @Mock
    private UserRequestRepo repository;

    @InjectMocks
    private UserRequestServiceImpl service;

//    @Test
//    public void shouldCreate(){
//        //Arrange
//        String username = "alex";
//        int id = 1;
//        UserRequest userRequest = new UserRequest(username,id);
//        //Act
//        service.create(username,id);
//        //Assert
//        verify(repository, times(1)).create(userRequest);
//    }


    @Test
    public void shouldGetAllTeacherRequests(){
        //Act
        service.getAllTeacherRequests();
        //Assert
        verify(repository, times(1)).getAllTeacherRequests();
    }

    @Test
    public void shouldGetAllAdminRequests(){
        //Act
        service.getAllAdminRequests();
        //Assert
        verify(repository, times(1)).getAllAdminRequests();
    }

    @Test
    public void shouldGetByUserAndRequest(){
        //Arrange
        String username = "alex";
        int id = 1;
        //Act
        service.getByUserAndRequest(username,id);
        //Assert
        verify(repository, times(1)).getByUserAndRequest(username,id);
    }

//    @Test
//    public void shouldApproveRequest(){
//        //Arrange
//        String username = "alex";
//        int requestType = 1;
//        UserRequest userRequest = new UserRequest(username,requestType);
//        //Act
//        service.approveRequest(username,1);
//        //Assert
//        verify(repository, times(1)).approveRequest(userRequest);
//    }

}
