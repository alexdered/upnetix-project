package com.example.teacherapp;

import com.example.teacherapp.models.User;
import com.example.teacherapp.repositories.StudentCourseRepo;
import com.example.teacherapp.repositories.UserRepo;
import com.example.teacherapp.services.StudentCourseServiceImpl;
import com.example.teacherapp.services.UserService;
import com.example.teacherapp.services.UserServiceImpl;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

@RunWith(MockitoJUnitRunner.class)
public class UserServiceImplTests {

    @Mock
    private UserRepo repository;

    @InjectMocks
    private UserServiceImpl service;

    @Test
    public void shouldCreateWhenValid(){
        //Arrange
        User user = new User("myFirst","myLast","myUser","myEmail@mail.com","myPic");
        //Act
        service.create(user);
        //Assert
        verify(repository, times(1)).create(user);
    }

    @Test
    public void shouldGetAll(){
        //Act
        service.getAll();
        //Assert
        verify(repository, times(1)).getAll();
    }

//    @Test
//    public void shouldUpdate(){
//        //Arrange
//        User user = new User("myFirst","myLast","myUser","myEmail@mail.com","myPic");
//        service.create(user);
//        //Act
//        service.updateUser(user);
//        //Assert
//        verify(repository, times(1)).updateUser(user);
//    }



}
