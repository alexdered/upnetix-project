package com.example.teacherapp;

import com.example.teacherapp.models.StudentCourse;
import com.example.teacherapp.repositories.StudentCourseRepo;
import com.example.teacherapp.services.StudentCourseServiceImpl;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;

import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

@RunWith(MockitoJUnitRunner.class)
public class StudentCourseServiceImplTests {

    @Mock
    private StudentCourseRepo repository;

    @InjectMocks
    private StudentCourseServiceImpl service;

//    @Test
//    public void shouldCreateStudentCourse(){
//
//        //Arrange
//        String username = "alex";
//        int id = 1;
//        StudentCourse studentCourse = new StudentCourse(username,id,0,1);
//        //Act
//        service.create(username,id);
//        //Assert
//        verify(repository, times(1)).create(studentCourse);
//    }

    @Test
    public void shouldGetAll(){
        //Act
        service.getAll();
        //Assert
        verify(repository, times(1)).getAll();
    }

    @Test
    public void shouldGetById(){
        //Arrange
        String username = "alex";
        int id = 1;
        //Act
        service.getById(username,id);
        //Assert
        verify(repository, times(1)).getById(username,id);
    }

    @Test
    public void shouldSaveStatus(){
        //Arrange
        StudentCourse studentCourse = new StudentCourse();
        int status = 1;
        //Act
        service.saveStatus(studentCourse,status);
        //Assert
        Mockito.verify(repository, times(1)).saveStatus(studentCourse,status);
    }

    @Test
    public void shouldSaveTotalGrade(){
        //Arrange
        StudentCourse studentCourse = new StudentCourse();
        int grade = 1;
        //Act
        service.saveStatus(studentCourse,grade);
        //Assert
        Mockito.verify(repository, times(1)).saveStatus(studentCourse,grade);
    }

}
