package com.example.teacherapp;

import com.example.teacherapp.repositories.RatingRepo;
import com.example.teacherapp.services.RatingServiceImpl;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;

@RunWith(MockitoJUnitRunner.class)
public class RatingServiceImplTests {

    @Mock
    private RatingRepo repository;

    @InjectMocks
    private RatingServiceImpl service;


//    @Test
//    public void shouldCreateRating(){
//        //Arrange
//        String username = "Alex";
//        int id = 38;
//        int rating = 4;
//        Rating rating1 = new Rating(username,id,rating);
//        //Act
//        service.createRatingForCourse(username,id,rating);
//        //Assert
//        Mockito.verify(repository, Mockito.times(1)).createRatingForCourse(rating1);
//    }

    @Test
    public void shouldGetAll(){
        //Act
        service.getAll();
        //Assert
        Mockito.verify(repository, Mockito.times(1)).getAll();
    }

    @Test
    public void shouldGetByCourseAndStudent(){
        //Arrange
        String username = "Alex";
        int id = 38;
        //Act
        service.getByCourseAndStudent(username,id);
        //Assert
        Mockito.verify(repository, Mockito.times(1)).getByCourseAndStudent(username,id);
    }


//    @Ignore
//    @Test
//    public void shouldGetAverageCourseGrade(){
//        //Arrange
//        int id = 38;
//        //Act
//        service.getAverageCourseRating(id);
//        //Assert
//        Mockito.verify(repository, Mockito.times(1)).getAverageCourseRating(id);
//    }


}
