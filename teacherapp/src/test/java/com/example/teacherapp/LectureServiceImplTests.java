package com.example.teacherapp;

import com.example.teacherapp.models.Lecture;
import com.example.teacherapp.repositories.LectureRepo;
import com.example.teacherapp.services.LectureServiceImpl;
import com.example.teacherapp.services.functional.YouTubeIdGenerator;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import static org.mockito.Mockito.*;

@RunWith(MockitoJUnitRunner.class)
public class LectureServiceImplTests {

    @Mock
    private LectureRepo lectureRepo;

    @Mock
    private YouTubeIdGenerator youTubeIdGenerator;

    @InjectMocks
    private LectureServiceImpl service;


    @Test
    public void create_Should_CreateInRepository_When_LectureIsValid() {

        //Arrange

        int randomCourseId = 1;
        String title = "New Lecture";
        String description = "Some description";
        String assignmentName = "course.png";

        when(youTubeIdGenerator.generateId("videoLink")).thenReturn("https://www.youtube.com/watch?v=CHyA3pp4G_Y");

        String videoName = youTubeIdGenerator.generateId("videoLink");

        Lecture lecture = new Lecture(randomCourseId, title, description, videoName, assignmentName);

        //Act
        service.create(lecture.getTitle(), lecture.getDescription(), lecture.getVideoName(), lecture.getAssignmentName(), lecture.getCourseId());

        //Assert

        verify(lectureRepo, times(1)).create(lecture);

    }
}
