package com.example.teacherapp;

import com.example.teacherapp.models.Homework;
import com.example.teacherapp.repositories.HomeworkRepo;
import com.example.teacherapp.services.HomeworkServiceImpl;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.ArrayList;
import java.util.List;

import static org.mockito.Mockito.*;


@RunWith(MockitoJUnitRunner.class)
public class HomeworkServiceImplTests {

    @Mock
    private HomeworkRepo homeworkRepo;

    @InjectMocks
    private HomeworkServiceImpl service;


    @Test
    public void create_Should_CreateInRepository_When_HomeworkIsValid() {

        //Arrange

        String studentUsername = "alex21";
        int courseId = 1;
        int lectureId = 1;
        String teacherUsername = "Bob Martin";
        String homeworkName = "homework.doc";

        int initGrade = 0;

        Homework homework = new Homework(studentUsername, courseId, lectureId, teacherUsername, homeworkName);

        //Act
        service.create(homework.getStudentUsername(), homework.getCourseId(), homework.getLectureId(), homework.getTeacherUsername(), homework.getHomeworkName(), initGrade);

        //Assert
        verify(homeworkRepo, times(1)).create(homework);

    }

    @Test
    public void getById_Should_ReturnHomework_When_HomeworkExists() {
        //Arrange
        int randomHomeworkId = 777;
        String studentUsername = "alex21";
        int lectureId = 1;
        String teacherUsername = "Bob Martin";
        String homeworkName = "homework.doc";

        Homework homeworkI = new Homework(studentUsername, 3, lectureId, teacherUsername, homeworkName);

        when(homeworkRepo.getById(randomHomeworkId)).thenReturn(homeworkI);

        //Act

        Homework actual = service.getById(randomHomeworkId);

        //Assert
        Assert.assertEquals(homeworkI, actual);

    }


        @Test
    public void getAllForTeacher_Should_ReturnAllTeacherHomeworkFromRepository(){

        String teacherUsername = "Bob Martin";
        String studentUsername = "alex21";
        int lectureId = 1;
        String homeworkName = "homework.doc";

        List<Homework> homeworkList = new ArrayList<>();
        Homework homeworkI = new Homework(studentUsername, 4, lectureId, teacherUsername, homeworkName);
        Homework homeworkII = new Homework(studentUsername,5 , lectureId, teacherUsername, homeworkName);

        homeworkList.add(homeworkI);
        homeworkList.add(homeworkII);

        List<Homework> expected = new ArrayList<>();
        expected.add(homeworkI);
        expected.add(homeworkII);

        when(homeworkRepo.getAllForTeacher(teacherUsername)).thenReturn(homeworkList);

        //Act

        List<Homework> actual = service.getAllForTeacher(teacherUsername);

        //Assert
        Assert.assertEquals(expected, actual);
    }


    @Test
    public void getAllForStudent_Should_ReturnAllStudentHomeworkFromRepository(){

        String studentUsername = "alex21";
        String teacherUsername = "Bob Martin";
        int lectureId = 1;
        String homeworkName = "homework.doc";

        List<Homework> homeworkList = new ArrayList<>();
        Homework homeworkI = new Homework(studentUsername, 4, lectureId, teacherUsername, homeworkName);
        Homework homeworkII = new Homework(studentUsername,5 , lectureId, teacherUsername, homeworkName);

        homeworkList.add(homeworkI);
        homeworkList.add(homeworkII);

        List<Homework> expected = new ArrayList<>();
        expected.add(homeworkI);
        expected.add(homeworkII);

        when(homeworkRepo.getAllForStudent(studentUsername)).thenReturn(homeworkList);

        //Act

        List<Homework> actual = service.getAllForStudent(studentUsername);

        //Assert
        Assert.assertEquals(expected, actual);
    }


    @Test
    public void existStudentByLecture_Should_ReturnTrue_When_StudentAndLectureIdAreRecorded(){
        String studentUsername = "alex21";
        String teacherUsername = "Bob Martin";
        int courseId= 1;
        int lectureId = 1;
        String homeworkName = "homework.doc";

        List<Homework> list= new ArrayList<>();
        Homework homeworkI = new Homework(studentUsername, courseId, lectureId, teacherUsername, homeworkName);
        list.add(homeworkI);

        when(homeworkRepo.getForStudentByLecture(studentUsername,lectureId)).thenReturn(list);

        //Act

        boolean actual = service.existStudentByLecture(studentUsername, lectureId);

        //Assert
        Assert.assertEquals(true, actual);
    }



    @Test
    public void existStudentByLecture_Should_ReturnFalse_When_StudentAndLectureIdAreNotRecorded(){
        String studentUsername = "alex21";
        String teacherUsername = "Bob Martin";
        int courseId= 1;
        int lectureId = 1;
        String homeworkName = "homework.doc";

        List<Homework> emptyList= new ArrayList<>();


        when(homeworkRepo.getForStudentByLecture(studentUsername,lectureId)).thenReturn(emptyList);

        //Act

        boolean actual = service.existStudentByLecture(studentUsername, lectureId);

        //Assert
        Assert.assertEquals(false, actual);
    }

    @Test
    public void update_Should_UpdateExistingHomewrok(){
        //Arrange

        String studentUsername = "alex21";
        int courseId = 1;
        int lectureId = 1;
        String teacherUsername = "Bob Martin";
        String homeworkName = "homework.doc";

        int initGrade = 0;

        Homework homework = new Homework(studentUsername, courseId, lectureId, teacherUsername, homeworkName);

        when(homeworkRepo.update(homework)).thenReturn(homework);
        //Act

        Homework actual = service.update(homework);

        //Assert
        Assert.assertEquals(homework, actual);

    }


    @Test
    public void grade_Should_UpdateInRepo_When_GradeIsAccurate(){

        //Arrange
        int randomId=1;
        int grade=75;
        String studentUsername = "alex21";
        int courseId = 1;
        int lectureId = 1;
        String teacherUsername = "Bob Martin";
        String homeworkName = "homework.doc";

        int initGrade = 0;

        Homework homework = new Homework(studentUsername, courseId, lectureId, teacherUsername, homeworkName);
        homework.setId(randomId);
        homework.setGrade(grade);


        when(homeworkRepo.grade(grade, randomId)).thenReturn(grade);

        //Act

        int actual = service.grade(grade, randomId);

        //Assert
        Assert.assertEquals(grade, actual);

    }



}
